﻿using System;
using Errors;

namespace InheritorsException
{
    /// <summary>
    /// Класс для передачи ошибок от Бд внутри сервиса
    /// </summary>
    [Serializable]
    public class ExceptionLocalDB : ApplicationException, IExceptionService
    {
        public EnumDBError Error { get; protected set; }

        public int IdClient { get; protected set; }

        public ExceptionLocalDB(int idClient, EnumDBError error) : base(DBError.GetDefenition(error))
        {
            Error = error;
            IdClient = idClient;
        }

        public ExceptionLocalDB(int idClient, string message) : base(message)
        {
            Error = EnumDBError.UnknownError;
            IdClient = idClient;
        }

    }
}
