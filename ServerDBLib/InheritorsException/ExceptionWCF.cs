﻿using Errors;
using System.Runtime.Serialization;

namespace InheritorsException
{
    /// <summary>
    /// Класс для передачи ошибок от Сервиса к Клиенту
    /// </summary>
    [DataContract]
    public class ExceptionWCF : IExceptionDb
    {
        [DataMember]
        public EnumDBError Error { get; protected set; }

        [DataMember]
        public string Message { get; protected set; }

        public ExceptionWCF(IExceptionDb exception)
        {
            Message = exception.Message;
            Error = exception.Error;
        }

        public ExceptionWCF(EnumDBError error)
        {
            Error = error;
            Message = DBError.GetDefenition(error);
        }

        public ExceptionWCF(string message)
        {
            Error = EnumDBError.UnknownError;
            Message = message;
        }
    }
}
