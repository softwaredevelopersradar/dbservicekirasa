﻿

namespace InheritorsException
{
    public interface IExceptionService : IExceptionDb
    {
        int IdClient { get; }
    }
}
