﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;

namespace KirasaModelsDBLib
{
    [DataContract]
    public class RadioIntelegence : INotifyPropertyChanged
    {
        private float frequencyAccuracy;
        private float bandAccuracy;
        private SamplingFrequency samplingFrequency = SamplingFrequency.f125e6;


        [DataMember]
        [NotifyParentProperty(true)]
        public float FrequencyAccuracy
        {
            get { return frequencyAccuracy; }
            set
            {
                if (value == frequencyAccuracy)
                    return;
                frequencyAccuracy = value;
                OnPropertyChanged();
            }
        }


        [DataMember]
        [NotifyParentProperty(true)]
        public float BandAccuracy
        {
            get { return bandAccuracy; }
            set
            {
                if (value == bandAccuracy)
                    return;
                bandAccuracy = value;
                OnPropertyChanged();
            }
        }


        [DataMember]
        [NotifyParentProperty(true)]
        public SamplingFrequency SamplingFrequency
        {
            get { return samplingFrequency; }
            set
            {
                if (value == samplingFrequency)
                    return;
                samplingFrequency = value;
                OnPropertyChanged();
            }
        }

        #region Methods
        public void Update(RadioIntelegence newRecord)
        {
            FrequencyAccuracy = newRecord.FrequencyAccuracy;
            BandAccuracy = newRecord.BandAccuracy;
            SamplingFrequency = newRecord.SamplingFrequency;
        }

        public RadioIntelegence Clone()
        {
            return new RadioIntelegence
            {
                FrequencyAccuracy = FrequencyAccuracy,
                BandAccuracy = BandAccuracy,
                SamplingFrequency = SamplingFrequency
            };
        }


        public bool EqualTo(RadioIntelegence data)
        {
            if ( frequencyAccuracy != data.FrequencyAccuracy || bandAccuracy != data.BandAccuracy || samplingFrequency != data.SamplingFrequency)
                return false;

            return true;
        }
        #endregion


        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }
        #endregion
    }
}
