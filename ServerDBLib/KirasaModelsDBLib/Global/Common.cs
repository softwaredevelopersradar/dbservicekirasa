﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;

namespace KirasaModelsDBLib
{
    [DataContract]
    public class Common : INotifyPropertyChanged
    {
        private short zoneAttention;
        private short zoneAlarm;
        private double centerLatitude;
        private double centerLongitude;
        private double operatorLatitude;
        private double operatorLongitude;

        [DataMember]
        [NotifyParentProperty(true)]
        public short ZoneAttention
        {
            get { return zoneAttention; }
            set
            {
                if (zoneAttention == value)
                    return;
                zoneAttention = value;
                OnPropertyChanged();
            }
        }


        [DataMember]
        [NotifyParentProperty(true)]
        public short ZoneAlarm
        {
            get => zoneAlarm;
            set
            {
                if (zoneAlarm == value) return;
                zoneAlarm = value;
                OnPropertyChanged();
            }
        }


        [DataMember]
        [NotifyParentProperty(true)]
        public double CenterLatitude
        {
            get => centerLatitude;
            set
            {
                if (centerLatitude == value) return;
                centerLatitude = value;
                OnPropertyChanged();
            }
        }


        [DataMember]
        [NotifyParentProperty(true)]
        public double CenterLongitude
        {
            get => centerLongitude;
            set
            {
                if (centerLongitude == value) return;
                centerLongitude = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [NotifyParentProperty(true)]
        public double OperatorLatitude
        {
            get => operatorLatitude;
            set
            {
                if (operatorLatitude == value) return;
                operatorLatitude = value;
                OnPropertyChanged();
            }
        }


        [DataMember]
        [NotifyParentProperty(true)]
        public double OperatorLongitude
        {
            get => operatorLongitude;
            set
            {
                if (operatorLongitude == value) return;
                operatorLongitude = value;
                OnPropertyChanged();
            }
        }


        public void Update(Common newRecord)
        {
            ZoneAttention = newRecord.ZoneAttention;
            ZoneAlarm = newRecord.ZoneAlarm;
            CenterLatitude = newRecord.CenterLatitude;
            CenterLongitude = newRecord.CenterLongitude;
            OperatorLatitude = newRecord.OperatorLatitude;
            OperatorLongitude = newRecord.OperatorLongitude;
        }

        public Common Clone()
        {
            return new Common
            {
                ZoneAttention = ZoneAttention,
                ZoneAlarm = ZoneAlarm,
                CenterLongitude = CenterLongitude,
                CenterLatitude = CenterLatitude,
                OperatorLatitude = OperatorLatitude,
                OperatorLongitude = OperatorLongitude
            };
        }


        public bool EqualTo(Common data)
        {
            if (centerLatitude != data.CenterLatitude || centerLongitude != data.CenterLongitude
                || zoneAttention != data.ZoneAttention || zoneAlarm != data.ZoneAlarm
                || operatorLatitude != data.OperatorLatitude || operatorLongitude != data.OperatorLongitude)
                return false;

            return true;
        }



        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }
        #endregion
}

}
