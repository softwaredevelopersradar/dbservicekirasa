﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;

namespace KirasaModelsDBLib
{
    [DataContract]
    public class DRMFilters : INotifyPropertyChanged
    {
        private bool isImaginarySolution;
        private short startElevation;
        private float alpha;

        [DataMember]
        [NotifyParentProperty(true)]
        public bool IsImaginarySolution
        {
            get { return isImaginarySolution; }
            set
            {
                if (value == isImaginarySolution)
                    return;
                isImaginarySolution = value;
                OnPropertyChanged();
            }
        }


        [DataMember]
        [NotifyParentProperty(true)]
        public short StartElevation
        {
            get { return startElevation; }
            set
            {
                if (startElevation == value)
                    return;
                startElevation = value;
                OnPropertyChanged();
            }
        }


        [DataMember]
        [NotifyParentProperty(true)]
        public float Alpha
        {
            get => alpha;
            set
            {
                if (alpha == value) return;
                alpha = value;
                OnPropertyChanged();
            }
        }



        #region Methods
        public void Update(DRMFilters newRecord)
        {
            StartElevation = newRecord.StartElevation;
            Alpha = newRecord.Alpha;
            IsImaginarySolution = newRecord.IsImaginarySolution;
        }

        public DRMFilters Clone()
        {
            return new DRMFilters
            {
                StartElevation = StartElevation,
                IsImaginarySolution = IsImaginarySolution,
                Alpha = Alpha
            };
        }


        public bool EqualTo(DRMFilters data)
        {
            if ( alpha != data.Alpha || startElevation != data.StartElevation || isImaginarySolution != data.IsImaginarySolution)
                return false;

            return true;
        }
        #endregion


        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }
        #endregion
    }
}
