﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;

namespace KirasaModelsDBLib
{
    [DataContract]
    public class KalmanFilterCoordinates : INotifyPropertyChanged
    {
        private float speedUavMax;
        private float timeHoldToDetect;
        private float maxResetTime;
        private short minAirObjPoints;        
        private float rmsX;
        private float rmsY;
        private float rmsZ;
        private bool staticErrorMatrix;
        private float coordinateDefinitionError;
        private float strobeDistance;
        private short tauError;
        private short xyzError;


        [DataMember]
        [NotifyParentProperty(true)]
        public float SpeedUavMax
        {
            get { return speedUavMax; }
            set
            {
                if (speedUavMax == value)
                    return;
                speedUavMax = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [NotifyParentProperty(true)]
        public float TimeHoldToDetect
        {
            get { return timeHoldToDetect; }
            set
            {
                if (timeHoldToDetect == value)
                    return;
                timeHoldToDetect = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [NotifyParentProperty(true)]
        public float MaxResetTime
        {
            get { return maxResetTime; }
            set
            {
                if (maxResetTime == value)
                    return;
                maxResetTime = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [NotifyParentProperty(true)]
        public short MinAirObjPoints
        {
            get { return minAirObjPoints; }
            set
            {
                if (minAirObjPoints == value)
                    return;
                minAirObjPoints = value;
                OnPropertyChanged();
            }
        }
        
        [DataMember]
        [NotifyParentProperty(true)]
        public float RmsX
        {
            get { return rmsX; }
            set
            {
                if (rmsX == value)
                    return;
                rmsX = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [NotifyParentProperty(true)]
        public float RmsY
        {
            get { return rmsY; }
            set
            {
                if (rmsY == value)
                    return;
                rmsY = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [NotifyParentProperty(true)]
        public float RmsZ
        {
            get { return rmsZ; }
            set
            {
                if (rmsZ == value)
                    return;
                rmsZ = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [NotifyParentProperty(true)]
        public bool StaticErrorMatrix
        {
            get { return staticErrorMatrix; }
            set
            {
                if (staticErrorMatrix == value)
                    return;
                staticErrorMatrix = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [NotifyParentProperty(true)]
        public float CoordinateDefinitionError
        {
            get { return coordinateDefinitionError; }
            set
            {
                if (coordinateDefinitionError == value)
                    return;
                coordinateDefinitionError = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [NotifyParentProperty(true)]
        public float StrobeDistance
        {
            get { return strobeDistance; }
            set
            {
                if (strobeDistance == value)
                    return;
                strobeDistance = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [NotifyParentProperty(true)]
        public short TauError
        {
            get { return tauError; }
            set
            {
                if (tauError == value)
                    return;
                tauError = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [NotifyParentProperty(true)]
        public short XyzError
        {
            get { return xyzError; }
            set
            {
                if (xyzError == value)
                    return;
                xyzError = value;
                OnPropertyChanged();
            }
        }
        #region Methods

        public void Update(KalmanFilterCoordinates newRecord)
        {
            SpeedUavMax = newRecord.SpeedUavMax;
            TimeHoldToDetect = newRecord.TimeHoldToDetect;
            MaxResetTime = newRecord.MaxResetTime;
            MinAirObjPoints = newRecord.MinAirObjPoints;
            RmsX = newRecord.RmsX;
            RmsY = newRecord.RmsY;
            RmsZ = newRecord.RmsZ;
            StaticErrorMatrix = newRecord.StaticErrorMatrix;
            CoordinateDefinitionError = newRecord.CoordinateDefinitionError;
            StrobeDistance = newRecord.StrobeDistance;
            TauError = newRecord.TauError;
            XyzError = newRecord.XyzError;
        }

        public KalmanFilterCoordinates Clone()
        {
            return new KalmanFilterCoordinates
            {
                SpeedUavMax = SpeedUavMax,
                TimeHoldToDetect = TimeHoldToDetect,
                MaxResetTime = MaxResetTime,
                MinAirObjPoints = MinAirObjPoints,
                RmsX = RmsX,
                RmsY = RmsY,
                RmsZ = RmsZ,
                StaticErrorMatrix = StaticErrorMatrix,
                CoordinateDefinitionError = CoordinateDefinitionError,
                StrobeDistance = StrobeDistance,
                TauError = TauError,
                XyzError = XyzError
            };
        }

        public bool EqualTo(KalmanFilterCoordinates data)
        {
            if (speedUavMax != data.SpeedUavMax || timeHoldToDetect != data.TimeHoldToDetect
                || maxResetTime != data.MaxResetTime || minAirObjPoints != data.MinAirObjPoints 
                || rmsX != data.RmsX || rmsY != data.RmsY || rmsZ != data.RmsZ
                || staticErrorMatrix != data.StaticErrorMatrix || coordinateDefinitionError != data.CoordinateDefinitionError || strobeDistance != data.StrobeDistance 
                || tauError != data.TauError || xyzError != data.XyzError)
                return false;

            return true;
        }
        #endregion


        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }
        #endregion
    }
}
