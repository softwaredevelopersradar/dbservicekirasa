﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;

namespace KirasaModelsDBLib
{
    [DataContract]
    public class Correlation
    {
        private byte amountCorrelationQuery;
        private float pathThroughCorrelationThreshold;
        private float coefficientBorderDеlayEstimation;


        [DataMember]
        [NotifyParentProperty(true)]
        public byte AmountCorrelationQuery
        {
            get { return amountCorrelationQuery; }
            set
            {
                if (value == amountCorrelationQuery)
                    return;
                amountCorrelationQuery = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [NotifyParentProperty(true)]
        public float PathThroughCorrelationThreshold
        {
            get { return pathThroughCorrelationThreshold; }
            set
            {
                if (value == pathThroughCorrelationThreshold)
                    return;
                pathThroughCorrelationThreshold = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [NotifyParentProperty(true)]
        public float CoefficientBorderDеlayEstimation
        {
            get { return coefficientBorderDеlayEstimation; }
            set
            {
                if (value == coefficientBorderDеlayEstimation)
                    return;
                coefficientBorderDеlayEstimation = value;
                OnPropertyChanged();
            }
        }


        #region Methods
        public void Update(Correlation newRecord)
        {
            AmountCorrelationQuery = newRecord.AmountCorrelationQuery;
            PathThroughCorrelationThreshold = newRecord.PathThroughCorrelationThreshold;
            CoefficientBorderDеlayEstimation = newRecord.CoefficientBorderDеlayEstimation;
        }


        public Correlation Clone()
        {
            return new Correlation
            {
                AmountCorrelationQuery = AmountCorrelationQuery,
                PathThroughCorrelationThreshold = PathThroughCorrelationThreshold,
                CoefficientBorderDеlayEstimation = CoefficientBorderDеlayEstimation
            };
        }


        public bool EqualTo(Correlation data)
        {
            if ( amountCorrelationQuery != data.AmountCorrelationQuery 
                || pathThroughCorrelationThreshold != data.PathThroughCorrelationThreshold 
                || coefficientBorderDеlayEstimation != CoefficientBorderDеlayEstimation)
                return false;

            return true;
        }

        #endregion


        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }
        #endregion
    }
}
