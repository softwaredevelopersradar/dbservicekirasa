﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Windows.Controls.WpfPropertyGrid;

namespace KirasaModelsDBLib
{
    [DataContract]
    [KnownType(typeof(AbstractCommonTable))]
    [InfoTable(NameTable.GlobalProperties)]

    public class GlobalProperties : AbstractCommonTable, INotifyPropertyChanged
    {
        [DataMember]
        [Browsable(false)]
        public override int Id { get; set; }

        [DataMember]
        [NotifyParentProperty(true)]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public Common Common { get; set; }

        [DataMember]
        [NotifyParentProperty(true)]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public RadioIntelegence RadioIntelegence { get; set; }

        [DataMember]
        [NotifyParentProperty(true)]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public KalmanFilterDelays KalmanFilterForDelay { get; set; }

        [DataMember]
        [NotifyParentProperty(true)]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public KalmanFilterCoordinates KalmanFilterForCoordinate { get; set; }

        [DataMember]
        [NotifyParentProperty(true)]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public Correlation Correlation { get; set; }

        [DataMember]
        [NotifyParentProperty(true)]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public DRMFilters DRMFilters { get; set; }

        public GlobalProperties()
        {
            Common = new Common();
            RadioIntelegence = new RadioIntelegence();
            KalmanFilterForCoordinate = new KalmanFilterCoordinates();
            KalmanFilterForDelay = new KalmanFilterDelays();
            Correlation = new Correlation();
            DRMFilters = new DRMFilters();

            RadioIntelegence.PropertyChanged += PropertyChanged;
            Common.PropertyChanged += PropertyChanged;
            KalmanFilterForCoordinate.PropertyChanged += PropertyChanged;
            KalmanFilterForDelay.PropertyChanged += PropertyChanged;
            Correlation.PropertyChanged += PropertyChanged;
            DRMFilters.PropertyChanged += PropertyChanged;
        }
        

        #region Methods
        public override object[] GetKey()
        {
            return new object[] { Id };
        }


        public override void Update(AbstractCommonTable record)
        {
            var newRecord = (GlobalProperties)record;
            Common.Update(newRecord.Common);
            RadioIntelegence.Update(newRecord.RadioIntelegence);
            KalmanFilterForCoordinate.Update(newRecord.KalmanFilterForCoordinate);
            KalmanFilterForDelay.Update(newRecord.KalmanFilterForDelay);
            Correlation.Update(newRecord.Correlation);
            DRMFilters.Update(newRecord.DRMFilters);
        }

        public GlobalProperties Clone()
        {
            return new GlobalProperties
            {
                Id = Id,
                Common = Common.Clone(),
                RadioIntelegence = RadioIntelegence.Clone(),
                KalmanFilterForCoordinate = KalmanFilterForCoordinate.Clone(),
                KalmanFilterForDelay = KalmanFilterForDelay.Clone(),
                Correlation = Correlation.Clone(),
                DRMFilters = DRMFilters.Clone()
            };
        }


        public bool EqualTo(GlobalProperties data)
        {
            return Common.EqualTo(data.Common) && RadioIntelegence.EqualTo(data.RadioIntelegence)
                && KalmanFilterForDelay.EqualTo(data.KalmanFilterForDelay) && KalmanFilterForCoordinate.EqualTo(data.KalmanFilterForCoordinate)
                && Correlation.EqualTo(data.Correlation) && DRMFilters.EqualTo(data.DRMFilters);
        }
        #endregion


        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }
        #endregion
    }
}
