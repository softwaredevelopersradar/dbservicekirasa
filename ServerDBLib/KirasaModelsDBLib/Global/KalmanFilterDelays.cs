﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;

namespace KirasaModelsDBLib
{
    [DataContract]
    public class KalmanFilterDelays : INotifyPropertyChanged
    {
        private bool staticErrorMatrix;
        private float delayMeasurementError;
        private float accelerationNoise;
        private float timeHoldToDetect;
        private float maxResetTime;
        private short minAirObjPoints;


        [DataMember]
        [NotifyParentProperty(true)]
        public bool StaticErrorMatrix
        {
            get { return staticErrorMatrix; }
            set
            {
                if (staticErrorMatrix == value)
                    return;
                staticErrorMatrix = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [NotifyParentProperty(true)]
        public float DelayMeasurementError
        {
            get { return delayMeasurementError; }
            set
            {
                if (value == delayMeasurementError)
                    return;
                delayMeasurementError = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [NotifyParentProperty(true)]
        public float AccelerationNoise
        {
            get { return accelerationNoise; }
            set
            {
                if (value == accelerationNoise)
                    return;
                accelerationNoise = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [NotifyParentProperty(true)]
        public float TimeHoldToDetect
        {
            get { return timeHoldToDetect; }
            set
            {
                if (timeHoldToDetect == value)
                    return;
                timeHoldToDetect = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [NotifyParentProperty(true)]
        public float MaxResetTime
        {
            get { return maxResetTime; }
            set
            {
                if (maxResetTime == value)
                    return;
                maxResetTime = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [NotifyParentProperty(true)]
        public short MinAirObjPoints
        {
            get { return minAirObjPoints; }
            set
            {
                if (minAirObjPoints == value)
                    return;
                minAirObjPoints = value;
                OnPropertyChanged();
            }
        }


        #region Methods

        public void Update(KalmanFilterDelays newRecord)
        {
            StaticErrorMatrix = newRecord.StaticErrorMatrix;
            DelayMeasurementError = newRecord.DelayMeasurementError;
            AccelerationNoise = newRecord.AccelerationNoise;
            TimeHoldToDetect = newRecord.TimeHoldToDetect;
            MaxResetTime = newRecord.MaxResetTime;
            MinAirObjPoints = newRecord.MinAirObjPoints;
        }

        public KalmanFilterDelays Clone()
        {
            return new KalmanFilterDelays
            {
                StaticErrorMatrix = StaticErrorMatrix,
                DelayMeasurementError = DelayMeasurementError,
                AccelerationNoise = AccelerationNoise,
                TimeHoldToDetect = TimeHoldToDetect,
                MaxResetTime = MaxResetTime,
                MinAirObjPoints = MinAirObjPoints
            };
        }

        public bool EqualTo(KalmanFilterDelays data)
        {
            if (staticErrorMatrix != data.StaticErrorMatrix || delayMeasurementError != data.DelayMeasurementError
                || accelerationNoise != data.AccelerationNoise || timeHoldToDetect != data.TimeHoldToDetect
                || maxResetTime != data.MaxResetTime || minAirObjPoints != data.MinAirObjPoints)
                return false;

            return true;
        }
        #endregion

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }
        #endregion
    }
}
