﻿namespace KirasaModelsDBLib.Interfaces
{
    public interface IFixReceivingPoint
    {
        int Id { get; set; }
        float Antenna { get; set; }
        Coord Coordinates { get; set; }
    }
}
