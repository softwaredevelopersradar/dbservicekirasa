﻿
namespace KirasaModelsDBLib.Interfaces
{
    public interface IFixFreqRanges
    {
        int Id { get; set; }

        double FreqMinKHz { get; set; }  // частота мин.

        double FreqMaxKHz { get; set; }  // частота макс.
    }
}
