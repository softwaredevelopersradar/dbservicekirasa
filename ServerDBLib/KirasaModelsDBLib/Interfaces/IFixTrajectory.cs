﻿
namespace KirasaModelsDBLib.Interfaces
{
    public interface IFixTrajectory
    {
        int Id { get; set; }
        short Num { get; set; }
        Coord Coordinates { get; set; }
        float Elevation { get; set; }
    }
}
