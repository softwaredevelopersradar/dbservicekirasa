﻿using System.Runtime.Serialization;

namespace KirasaModelsDBLib
{
    /// <summary>
    /// Удаленные пункты приема
    /// </summary>
    [DataContract]
    [KnownType(typeof(TableReceivingPoint))]
    [InfoTable(NameTable.TableRemotePoints)]
    public class TableRemotePoints : TableReceivingPoint
    {
    }
}
