﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Windows.Controls.WpfPropertyGrid;
using KirasaModelsDBLib.Interfaces;

namespace KirasaModelsDBLib
{
    /// <summary>
    /// Пункты приема
    /// </summary>
    [DataContract]
    [CategoryOrder("ID", 1)]
    [CategoryOrder(nameof(Coordinates), 2)]
    [CategoryOrder("Прочее", 3)]
    [KnownType(typeof(TableLocalPoints))]
    [KnownType(typeof(TableRemotePoints))]
    public class TableReceivingPoint : AbstractCommonTable, IFixReceivingPoint
    {
        [DataMember]
        [Category("ID")]
        [DisplayName(nameof(Id))]
        [Browsable(true)]
        public override int Id { get; set; }

        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(Antenna))]
        public float Antenna { get; set; }

        [DataMember]
        [Category(nameof(Coordinates))]
        [DisplayName(" ")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public Coord Coordinates { get; set; } = new Coord();

        [DataMember]
        [Category("ID")]
        [DisplayName(nameof(IsCetral))]
        [Browsable(false)]
        public bool IsCetral { get; set; }


        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(TimeError))]
        public float TimeError { get; set; }


        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(Note))]
        public string Note { get; set; } = string.Empty;

        public override object[] GetKey()
        {
            return new object[] { Id };
        }

        public override void Update(AbstractCommonTable record)
        {
            Id = ((TableReceivingPoint)record).Id;
            Coordinates.Altitude = ((TableReceivingPoint)record).Coordinates.Altitude;
            Coordinates.Latitude = ((TableReceivingPoint)record).Coordinates.Latitude;
            Coordinates.Longitude = ((TableReceivingPoint)record).Coordinates.Longitude;
            Antenna = ((TableReceivingPoint)record).Antenna;
            IsCetral = ((TableReceivingPoint)record).IsCetral;
            Note = ((TableReceivingPoint)record).Note;
            TimeError = ((TableReceivingPoint)record).TimeError;
        }

        public TableReceivingPoint Clone()
        {
            return new TableReceivingPoint
            {
                Id = this.Id,
                Coordinates = new Coord()
                {
                    Altitude = Coordinates.Altitude,
                    Longitude = Coordinates.Longitude,
                    Latitude = Coordinates.Latitude,
                },
                Antenna = Antenna,
                IsCetral = IsCetral,
                Note = Note,
                TimeError = TimeError
            };
        }

        public TableLocalPoints ToLocalPoint()
        {
            TableLocalPoints table = new TableLocalPoints()
            {
                Id = this.Id,
                Coordinates = new Coord()
                {
                    Altitude = Coordinates.Altitude,
                    Longitude = Coordinates.Longitude,
                    Latitude = Coordinates.Latitude,
                },
                Antenna = this.Antenna,
                IsCetral = this.IsCetral,
                Note = this.Note,
                TimeError = this.TimeError
            };
            return table;
        }

        public TableRemotePoints ToRemotePoint()
        {
            TableRemotePoints table = new TableRemotePoints()
            {
                Id = this.Id,
                Coordinates = new Coord()
                {
                    Altitude = Coordinates.Altitude,
                    Longitude = Coordinates.Longitude,
                    Latitude = Coordinates.Latitude,
                },
                Antenna = this.Antenna,
                IsCetral = this.IsCetral,
                Note = this.Note,
                TimeError = this.TimeError
            };
            return table;
        }
    }
}
