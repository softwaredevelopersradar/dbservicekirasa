﻿using System.ComponentModel;
using System.Runtime.Serialization;
using System.Windows.Controls.WpfPropertyGrid;

namespace KirasaModelsDBLib
{
    /// <summary>
    /// Пункты приема
    /// </summary>
    [DataContract]
    [CategoryOrder("ID", 1)]
    [CategoryOrder(nameof(Coordinates), 2)]
    [CategoryOrder("Прочее", 3)]
    [KnownType(typeof(AbstractCommonTable))]
    [InfoTable(NameTable.TableOtherPoints)]
    public class TableOtherPoints : AbstractCommonTable
    {
        [DataMember]
        [Category("ID")]
        [DisplayName(nameof(Id))]
        [Browsable(false)]
        public override int Id { get; set; }

        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(Type))]
        public TypeOtherPoints Type { get; set; }

        [DataMember]
        [Category(nameof(Coordinates))]
        [DisplayName(" ")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public Coord Coordinates { get; set; } = new Coord();


        public override object[] GetKey()
        {
            return new object[] { Id };
        }

        public override void Update(AbstractCommonTable record)
        {
            Id = ((TableOtherPoints)record).Id;
            Type = ((TableOtherPoints)record).Type;
            Coordinates.Altitude = ((TableOtherPoints)record).Coordinates.Altitude;
            Coordinates.Latitude = ((TableOtherPoints)record).Coordinates.Latitude;
            Coordinates.Longitude = ((TableOtherPoints)record).Coordinates.Longitude;

        }

        public TableOtherPoints Clone()
        {
            return new TableOtherPoints
            {
                Id = this.Id,
                Coordinates = new Coord()
                {
                    Altitude = Coordinates.Altitude,
                    Longitude = Coordinates.Longitude,
                    Latitude = Coordinates.Latitude,
                },
                Type = Type
            };
        }
    }
}
