﻿using System.Runtime.Serialization;

namespace KirasaModelsDBLib
{
    /// <summary>
    /// Локальные пункты приема
    /// </summary>
    [DataContract]
    [KnownType(typeof(TableReceivingPoint))]
    [InfoTable(NameTable.TableLocalPoints)]

    public class TableLocalPoints : TableReceivingPoint
    {
    }
}
