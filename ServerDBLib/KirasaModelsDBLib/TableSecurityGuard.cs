﻿using System.ComponentModel;
using System.Runtime.Serialization;
using System.Windows.Controls.WpfPropertyGrid;

namespace KirasaModelsDBLib
{
    /// <summary>
    /// Охранники
    /// </summary>
    [DataContract]
    [CategoryOrder("ID", 1)]
    [CategoryOrder(nameof(Coordinates), 2)]
    [CategoryOrder("Прочее", 3)]
    [KnownType(typeof(AbstractCommonTable))]
    [InfoTable(NameTable.TableSecurityGuard)]
    public class TableSecurityGuard : AbstractCommonTable
    {
        [DataMember]
        [Category("ID")]
        [DisplayName(nameof(Id))]
        [Browsable(true)]
        public override int Id { get; set; }

        [DataMember]
        [Category(nameof(Coordinates))]
        [DisplayName(" ")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public Coord Coordinates { get; set; } = new Coord();

        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(Note))]
        public string GuardStation { get; set; } = string.Empty;

        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(Note))]
        public string Note { get; set; } = string.Empty;

        public TableSecurityGuard()
        {}

        public TableSecurityGuard(Coord coord, string guardStation, string note, int id = 0)
        {
            this.Coordinates = coord.Clone();
            this.GuardStation = guardStation;
            this.Note = note;
            this.Id = id;
        }

        public override object[] GetKey()
        {
            return new object[] { Id };
        }

        public override void Update(AbstractCommonTable rec)
        {
            var record = (TableSecurityGuard)rec;

            this.Id = record.Id;
            this.Coordinates.Update(record.Coordinates);
            this.GuardStation = record.GuardStation;
            this.Note = record.Note;
        }

        public TableSecurityGuard Clone() => new TableSecurityGuard(Coordinates, GuardStation, Note, Id);

    }
}
