﻿using System.Runtime.Serialization;

namespace KirasaModelsDBLib
{
    /// <summary>
    /// Известные частоты (ИЧ)
    /// </summary>
    [DataContract]
    [KnownType(typeof(TableFreqRanges))]
    [InfoTable(NameTable.TableFreqKnown)]

    public class TableFreqKnown : TableFreqRanges
    {
    }
}
