﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Windows.Controls.WpfPropertyGrid;
using KirasaModelsDBLib.Interfaces;

namespace KirasaModelsDBLib
{
    /// <summary>
    /// Диапазоны частот
    /// </summary>
    [DataContract]
    [CategoryOrder("ID", 1)]
    [CategoryOrder("Диапазон", 2)]
    [CategoryOrder("Прочее", 3)]
    [KnownType(typeof(TableFreqRangesRecon))]
    [KnownType(typeof(TableFreqKnown))]

    public class TableFreqRanges : AbstractCommonTable, IFixFreqRanges
    {
        [DataMember]
        [Category("ID")]
        [DisplayName(nameof(Id)), Browsable(false)]
        public override int Id { get; set; }

        [DataMember]
        [Category("Диапазон")]
        [PropertyOrder(1)]
        //[DisplayName("F мин., кГц")]
        [DisplayName(nameof(FreqMinKHz))]
        public double FreqMinKHz { get; set; }  // частота мин.

        [DataMember]
        [Category("Диапазон")]
        [PropertyOrder(2)]
        //[DisplayName("F макс., кГц")]
        [DisplayName(nameof(FreqMaxKHz))]
        public double FreqMaxKHz { get; set; }  // частота макс.

        [DataMember]
        [Category("Прочее")]
        //[DisplayName("Примечание")]
        [DisplayName(nameof(Note))]
        public string Note { get; set; } = string.Empty; // примечание

        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(IsActive)), Browsable(false)]
        public bool IsActive { get; set; }  

        public TableFreqRanges Clone()
        {
            return new TableFreqRanges
            {
                Id = this.Id,
                FreqMinKHz = this.FreqMinKHz,
                FreqMaxKHz = this.FreqMaxKHz,
                Note = this.Note,
                IsActive = this.IsActive
            };
        }

        public override object[] GetKey()
        {
            return new object[] { Id };
        }

        public TableFreqRangesRecon ToRangesRecon()
        {
            TableFreqRangesRecon table = new TableFreqRangesRecon()
            {
                Id = this.Id,
                FreqMaxKHz = this.FreqMaxKHz,
                FreqMinKHz = this.FreqMinKHz,
                Note = this.Note,
                IsActive = this.IsActive
            };
            return table;
        }

        public TableFreqKnown ToFreqKnown()
        {
            TableFreqKnown table = new TableFreqKnown()
            {
                Id = this.Id,
                FreqMaxKHz = this.FreqMaxKHz,
                FreqMinKHz = this.FreqMinKHz,
                Note = this.Note,
                IsActive = this.IsActive
            };
            return table;
        }

        public override void Update(AbstractCommonTable record)
        {
            
            FreqMaxKHz = ((TableFreqRanges)record).FreqMaxKHz;
            FreqMinKHz = ((TableFreqRanges)record).FreqMinKHz;
            Note = ((TableFreqRanges)record).Note;
            IsActive = ((TableFreqRanges)record).IsActive;
        }
    }
}
