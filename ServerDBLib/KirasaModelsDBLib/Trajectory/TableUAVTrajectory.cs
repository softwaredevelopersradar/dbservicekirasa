﻿using System.Runtime.Serialization;

namespace KirasaModelsDBLib
{
    /// <summary>
    /// Траектория ИРИ БПЛА
    /// </summary>
    [DataContract]
    [KnownType(typeof(TableTrajectory))]
    [InfoTable(NameTable.TableUAVTrajectory)]
    public class TableUAVTrajectory : TableTrajectory
    {
    }
}
