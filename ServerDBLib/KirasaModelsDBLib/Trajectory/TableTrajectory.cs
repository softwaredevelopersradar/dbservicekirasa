﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Windows.Controls.WpfPropertyGrid;
using KirasaModelsDBLib.Interfaces;

namespace KirasaModelsDBLib
{
    /// <summary>
    /// Траектория ИРИ БПЛА
    /// </summary>
    [DataContract]
    [CategoryOrder("ID", 1)]
    [CategoryOrder("Общие", 2)]
    [CategoryOrder(nameof(Coordinates), 3)]
    [CategoryOrder("Прочее", 4)]
    [KnownType(typeof(TableUAVTrajectory))]
    [KnownType(typeof(TableUAVTrajectoryArchive))]
    public class TableTrajectory : AbstractCommonTable, IFixTrajectory
    {
        [DataMember]
        public override int Id { get; set; }

        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(Num))]
        public short Num { get; set; }

        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(FrequencyKHz))]
        public double FrequencyKHz { get; set; }

        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(BandKHz))]
        public float BandKHz { get; set; }

        [DataMember]
        [Category(nameof(Coordinates))]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public Coord Coordinates { get; set; } = new Coord();

        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(Elevation))]
        public float Elevation { get; set; }

        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(RadiusA))]
        public float RadiusA { get; set; }

        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(RadiusB))]
        public float RadiusB { get; set; }

        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(Head))]
        public short Head { get; set; }

        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(Time))]
        public DateTime Time { get; set; }

        [DataMember]
        public int TableUAVResId { get; set; }


        public override object[] GetKey()
        {
            return new object[] { Id };
        }

        public override void Update(AbstractCommonTable record)
        {
            Num = ((TableTrajectory)record).Num;
            FrequencyKHz = ((TableTrajectory)record).FrequencyKHz;
            BandKHz = ((TableTrajectory)record).BandKHz;
            Coordinates.Altitude = ((TableTrajectory)record).Coordinates.Altitude;
            Coordinates.Latitude = ((TableTrajectory)record).Coordinates.Latitude;
            Coordinates.Longitude = ((TableTrajectory)record).Coordinates.Longitude;
            Elevation = ((TableTrajectory)record).Elevation;
            RadiusA = ((TableTrajectory)record).RadiusA;
            RadiusB = ((TableTrajectory)record).RadiusB;
            Time = ((TableTrajectory)record).Time;
            Head = ((TableTrajectory)record).Head;
        }

        public TableUAVTrajectory ToUAVTrajectory()
        {
            return new TableUAVTrajectory()
            {
                Id = Id,
                Num = Num,
                FrequencyKHz = FrequencyKHz,
                BandKHz = BandKHz,
                Coordinates = new Coord()
                {
                    Altitude = Coordinates.Altitude,
                    Longitude = Coordinates.Longitude,
                    Latitude = Coordinates.Latitude,
                },
                Elevation = Elevation,
                RadiusA = RadiusA,
                RadiusB = RadiusB,
                Time = Time,
                Head = Head,
                TableUAVResId = TableUAVResId
            };
        }

        public TableUAVTrajectoryArchive ToUAVTrajectoryArchive()
        {
            return new TableUAVTrajectoryArchive()
            {
                Id = Id,
                Num = Num,
                FrequencyKHz = FrequencyKHz,
                BandKHz = BandKHz,
                Coordinates = new Coord()
                {
                    Altitude = Coordinates.Altitude,
                    Longitude = Coordinates.Longitude,
                    Latitude = Coordinates.Latitude,
                },
                Elevation = Elevation,
                RadiusA = RadiusA,
                RadiusB = RadiusB,
                Time = Time,
                Head = Head,
                TableUAVResId = TableUAVResId
            };
        }
    }
}
