﻿using System.Runtime.Serialization;

namespace KirasaModelsDBLib
{
    /// <summary>
     /// Траектория ИРИ БПЛА архив
     /// </summary>
    [DataContract]
    [KnownType(typeof(TableTrajectory))]
    [InfoTable(NameTable.TableUAVTrajectoryArchive)]
    public class TableUAVTrajectoryArchive : TableTrajectory
    {
    }
}
