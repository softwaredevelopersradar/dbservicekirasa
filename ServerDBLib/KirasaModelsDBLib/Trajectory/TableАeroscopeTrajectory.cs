﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Windows.Controls.WpfPropertyGrid;
using KirasaModelsDBLib.Interfaces;

namespace KirasaModelsDBLib
{
    /// <summary>
    /// Траектория А БПЛА
    /// </summary>
    [DataContract]
    [CategoryOrder("ID", 1)]
    [CategoryOrder("Общие", 2)]
    [CategoryOrder(nameof(Coordinates), 3)]
    [CategoryOrder("Прочее", 4)]
    //[KnownType(typeof(Coord))]
    [InfoTable(NameTable.TableАeroscopeTrajectory)]
    public class TableAeroscopeTrajectory : AbstractCommonTable, IFixTrajectory
    {
        [DataMember]
        public override int Id { get; set; }

        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(Num))]
        public short Num { get; set; }

        [DataMember]
        [Category(nameof(Coordinates))]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public Coord Coordinates { get; set; } = new Coord();

        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(Elevation))]
        public float Elevation { get; set; }

        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(Time))]
        public DateTime Time { get; set; }

        [DataMember]
        public string SerialNumber { get; set; } = string.Empty;

        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(Roll))]
        public float Roll { get; set; }

        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(Pitch))]
        public float Pitch { get; set; }

        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(Yaw))]
        public float Yaw { get; set; }

        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(V_up))]
        public float V_up { get; set; }

        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(V_east))]
        public float V_east { get; set; }

        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(V_north))]
        public float V_north { get; set; }

        public override object[] GetKey()
        {
            return new object[] { Id };
        }

        public override void Update(AbstractCommonTable record)
        {
            Num = ((TableUAVTrajectory)record).Num;
            Coordinates.Altitude = ((TableUAVTrajectory)record).Coordinates.Altitude;
            Coordinates.Latitude = ((TableUAVTrajectory)record).Coordinates.Latitude;
            Coordinates.Longitude = ((TableUAVTrajectory)record).Coordinates.Longitude;
            Elevation = ((TableUAVTrajectory)record).Elevation;
            Time = ((TableAeroscopeTrajectory)record).Time;
            Roll = ((TableAeroscopeTrajectory)record).Roll;
            Pitch = ((TableAeroscopeTrajectory)record).Pitch;
            Yaw = ((TableAeroscopeTrajectory)record).Yaw;
            V_up = ((TableAeroscopeTrajectory)record).V_up;
            V_east = ((TableAeroscopeTrajectory)record).V_east;
            V_north = ((TableAeroscopeTrajectory)record).V_north;
        }
    }
}
