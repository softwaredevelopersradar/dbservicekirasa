﻿using System.Runtime.Serialization;

namespace KirasaModelsDBLib
{
    [DataContract]
    [KnownType(typeof(TableAeroscope))]
    [KnownType(typeof(TableUAVRes))]
    [KnownType(typeof(TableAeroscopeTrajectory))]
    [KnownType(typeof(TableUAVTrajectory))]
    [KnownType(typeof(TableSignalsUAV))]
    [KnownType(typeof(TableOtherPoints))]
    [KnownType(typeof(TableSecurityGuard))]
    public abstract class AbstractCommonTable
    {
        [DataMember]
        public abstract int Id { get; set; }

        public abstract object[] GetKey();

        public abstract void Update(AbstractCommonTable record);
    }
}
