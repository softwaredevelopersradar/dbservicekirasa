﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KirasaModelsDBLib
{
    public class InfoTableAttribute : Attribute
    {
        public NameTable Name { get; private set; }

        public InfoTableAttribute(NameTable name)
        {
            Name = name;
        }
    }
}
