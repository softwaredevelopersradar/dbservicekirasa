﻿using System.ComponentModel;
using System.Runtime.Serialization;

namespace KirasaModelsDBLib
{
    [DataContract]
    public enum NameChangeOperation : byte
    {
        [EnumMember]
        Add,      // Добавить запись 
        [EnumMember]
        Delete,   // Удалить запись
        [EnumMember]
        Change   // Изменить запись
    }

    [DataContract]
    public enum NameTableOperation
    {
        [EnumMember]
        Add,
        [EnumMember]
        AddRange,
        [EnumMember]
        RemoveRange,
        [EnumMember]
        Change,
        [EnumMember]
        Delete,
        [EnumMember]
        Clear,
        [EnumMember]
        Load,
        //[EnumMember]
        //ClearByFilter,
        [EnumMember]
        Update,
        [EnumMember]
        None
    }

    [DataContract]
    public enum NameTable : byte
    {
        [EnumMember]
        [Description("Локальные пункты приема")]
        TableLocalPoints,

        [EnumMember]
        [Description("Удаленные пункты приема")]
        TableRemotePoints,

        [EnumMember]
        [Description("Диапазоны РР")]
        TableFreqRangesRecon, //Сектора и диапазоны РР  TableFreqRangesRecon

        [EnumMember]
        [Description("Известные частоты")]
        TableFreqKnown,

        [EnumMember]
        [Description("ИРИ БПЛА")]
        TableUAVRes,         

        [EnumMember]
        [Description("Аэроскоп БПЛА")]
        TableАeroscope,          

        [EnumMember]
        [Description("Траектория ИРИ БПЛА")]
        TableUAVTrajectory,   

        [EnumMember]
        [Description("Траектория А БПЛА")]
        TableАeroscopeTrajectory,   

        [EnumMember]
        [Description("Глобальные настройки")]
        GlobalProperties,

        [EnumMember]
        [Description("ИРИ БПЛА Архив")]
        TableUAVResArchive,

        [EnumMember]
        [Description("Траектория ИРИ БПЛА Архив")]
        TableUAVTrajectoryArchive,

        [EnumMember]
        [Description("Cигналы ИРИ")]
        TableSignalsUAV,

        [EnumMember]
        [Description("Другие средства")]
        TableOtherPoints,

        [EnumMember]
        [Description("Охранники")]
        TableSecurityGuard
    }

    public enum Languages
    {
        [Description("Русский")]
        Rus,
        [Description("English")]
        Eng
    }


    public enum TypeUAVRes : byte
    {
        [Description("Unknown")]
        Unknown,
        [Description("Lightbridge")]
        Lightbridge,
        [Description("Ocusinc")]
        Ocusinc,
        [Description("Wi-Fi (M)")]
        WiFi,
        [Description("3G")]
        G3
    }


    [DataContract]
    public enum TypeL : byte
    {
        [EnumMember]
        Friend,
        [EnumMember]
        Enemy
    }


    [DataContract]
    public enum TypeM : byte
    {
        [EnumMember]
        Unknown,
        [EnumMember]
        Drone
    }


    [DataContract]
    public enum TypeSystem : byte
    {
        [EnumMember]
        CP_CuirasseM,
        [EnumMember]
        Martsinovich,
        [EnumMember]
        Lyakh,
        [EnumMember]
        Manual,
        [EnumMember]
        Spectrum
    }


    [DataContract]
    public enum TypeOtherPoints : byte
    {
        [EnumMember]
        GrozaS,
    }


    [DataContract]
    public enum SamplingFrequency : int
    {
        [EnumMember]
        f31_25e6 = 31250000,
        [EnumMember]
        f62_5e6 = 62500000,
        [EnumMember]
        f125e6 = 125000000
    }
    //[DataContract]
    //public enum FileType : byte
    //{
    //    [EnumMember]
    //    Txt,
    //    [EnumMember]
    //    Word,
    //    [EnumMember]
    //    Excel
    //}
}
