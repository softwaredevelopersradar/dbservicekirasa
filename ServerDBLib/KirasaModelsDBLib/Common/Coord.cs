﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System;

namespace KirasaModelsDBLib
{
    /// <summary>
    /// Координаты
    /// </summary>
    [DataContract]
    public class Coord : INotifyPropertyChanged, IEquatable<Coord>
    {
        #region NotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }

        #endregion

        #region Private

        private double latitude = -1;
        private double longitude = -1;
        private float altitude = -1;

        #endregion

        [DataMember]
        [DisplayName(nameof(Latitude))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public double Latitude
        {
            get { return latitude; }
            set
            {
                if (latitude == value)
                    return;
                latitude = value;
                OnPropertyChanged(nameof(Latitude));
            }
        }

        [DataMember]
        [DisplayName(nameof(Longitude))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public double Longitude
        {
            get { return longitude; }
            set
            {
                if (longitude == value)
                    return;
                longitude = value;
                OnPropertyChanged(nameof(Longitude));
            }
        }

        [DataMember]
        [DisplayName(nameof(Altitude))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public float Altitude
        {
            get { return altitude; }
            set
            {
                if (altitude == value)
                    return;
                altitude = value;
                OnPropertyChanged(nameof(Altitude));
            }
        }

        public Coord() { }
        public Coord(double latitude, double longitude, float altitude)
        {
            Altitude = altitude;
            Latitude = latitude;
            Longitude = longitude;
        }

        public void Update(Coord record)
        {
            Altitude = record.Altitude;
            Latitude = record.Latitude;
            Longitude = record.Longitude;
        }

        public Coord Clone() => new Coord(Latitude, Longitude, Altitude);

        public override string ToString() //хз зачем
        {
            if ((Latitude < 0 && Longitude < 0 && Altitude < 0) || (Latitude < 0 || Longitude < 0))
                return " ";
            if (Altitude < 0)
                return $"{latitude} : {longitude} : — ";
            return $"{latitude} : {longitude} : {altitude}";
        }

        public bool Equals(Coord other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return this.latitude.Equals(other.latitude) && this.longitude.Equals(other.longitude) && this.altitude.Equals(other.altitude);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Coord)obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(this.latitude, this.longitude, this.altitude);
        }
    }
}
