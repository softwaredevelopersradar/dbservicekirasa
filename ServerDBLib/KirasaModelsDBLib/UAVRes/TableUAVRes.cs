﻿using System.Runtime.Serialization;

namespace KirasaModelsDBLib
{
    /// <summary>
    /// ИРИ БПЛА
    /// </summary>
    [DataContract]
    [KnownType(typeof(TableUAVBase))]
    [InfoTable(NameTable.TableUAVRes)]
    public class TableUAVRes : TableUAVBase
    {
    }
}
