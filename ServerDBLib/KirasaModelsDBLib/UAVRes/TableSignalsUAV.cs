﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Windows.Controls.WpfPropertyGrid;

namespace KirasaModelsDBLib
{
    [DataContract]
    [CategoryOrder("ID", 1)]
    [CategoryOrder("Общие", 2)]
    [CategoryOrder("Прочее", 3)]
    [KnownType(typeof(AbstractCommonTable))]
    [InfoTable(NameTable.TableSignalsUAV)]
    public class TableSignalsUAV : AbstractCommonTable
    {
        [DataMember]
        [Category("ID")]
        [DisplayName(nameof(Id))]
        [Browsable(false)]
        public override int Id { get; set; }


        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(FrequencyKHz))]
        public double FrequencyKHz { get; set; }

        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(BandKHz))]
        public float BandKHz { get; set; }

        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(Type))]
        public TypeUAVRes Type { get; set; }

        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(TypeM))]
        public TypeM TypeM { get; set; }

        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(TypeL))]
        public TypeL TypeL { get; set; }


        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(TimeStop))]
        public DateTime TimeStop { get; set; }


        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(TimeStart))]
        public DateTime TimeStart { get; set; }


        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(Correlation))]
        public bool Correlation { get; set; }

        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(System))]
        public TypeSystem System { get; set; }


        public override object[] GetKey()
        {
            return new object[] { Id };
        }

        public override void Update(AbstractCommonTable record)
        {
            FrequencyKHz = ((TableSignalsUAV)record).FrequencyKHz;
            BandKHz = ((TableSignalsUAV)record).BandKHz;
            Type = ((TableSignalsUAV)record).Type;
            TypeL = ((TableSignalsUAV)record).TypeL;
            TypeM = ((TableSignalsUAV)record).TypeM;
            TimeStop = ((TableSignalsUAV)record).TimeStop;
            TimeStart = ((TableSignalsUAV)record).TimeStart;
            Correlation = ((TableSignalsUAV)record).Correlation;
            System = ((TableSignalsUAV)record).System;
        }

        public TableSignalsUAV Clone()
        {
            return new TableSignalsUAV()
            {
                Id = Id,
                FrequencyKHz = FrequencyKHz,
                BandKHz= BandKHz,
                Type = Type,
                TypeL = TypeL,
                TypeM = TypeM,
                TimeStop = TimeStop,
                TimeStart = TimeStart,
                Correlation = Correlation,
                System = System
            };
        }
        
    }
}
