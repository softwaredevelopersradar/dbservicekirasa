﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Windows.Controls.WpfPropertyGrid;

namespace KirasaModelsDBLib
{
    [DataContract]
    [CategoryOrder("ID", 1)]
    [CategoryOrder("Общие", 2)]
    [CategoryOrder("Прочее", 3)]
    [KnownType(typeof(TableUAVRes))]
    [KnownType(typeof(TableUAVResArchive))]
    public class TableUAVBase : AbstractCommonTable
    {
        [DataMember]
        [Category("ID")]
        [DisplayName(nameof(Id))]
        [Browsable(false)]
        public override int Id { get; set; }


        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(Type))]
        public TypeUAVRes Type { get; set; }

        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(IsActive)), Browsable(false)]
        public bool IsActive { get; set; }


        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(State)), Browsable(false)]
        public byte State { get; set; }

        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(TypeL))]
        public TypeL TypeL { get; set; }

        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(TypeM))]
        public TypeM TypeM { get; set; }

        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(Flight))]
        public byte Flight { get; set; }


        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(TrackTime))]
        public DateTime TrackTime { get; set; }


        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(Note))]
        public string Note { get; set; } = string.Empty;

        public override object[] GetKey()
        {
            return new object[] { Id };
        }

        public override void Update(AbstractCommonTable record)
        {
            Type = ((TableUAVBase)record).Type;
            IsActive = ((TableUAVBase)record).IsActive;
            State = ((TableUAVBase)record).State;
            TrackTime = ((TableUAVBase)record).TrackTime;
            Note = ((TableUAVBase)record).Note;
            TypeL = ((TableUAVBase)record).TypeL;
            TypeM = ((TableUAVBase)record).TypeM;
            Flight = ((TableUAVBase)record).Flight;
        }

        public TableUAVBase Clone()
        {
            return new TableUAVBase()
            {
                Id = this.Id,
                Type = this.Type,
                IsActive = this.IsActive,
                State = this.State,
                TrackTime = TrackTime,
                Note = this.Note,
                TypeL =  this.TypeL,
                TypeM = this.TypeM,
                Flight = this.Flight
            };
        }


        public TableUAVRes ToUAVRes()
        {
            return new TableUAVRes()
            {
                Id = this.Id,
                Type = this.Type,
                IsActive = this.IsActive,
                State = this.State,
                TrackTime = TrackTime,
                Note = this.Note,
                TypeL = this.TypeL,
                TypeM = this.TypeM,
                Flight = this.Flight
            };
        }

        public TableUAVResArchive ToUAVResArchive()
        {
            return new TableUAVResArchive()
            {
                Id = this.Id,
                Type = this.Type,
                IsActive = this.IsActive,
                State = this.State,
                TrackTime = TrackTime,
                Note = this.Note,
                TypeL = this.TypeL,
                TypeM = this.TypeM,
                Flight = this.Flight
            };
        }

    }
}
