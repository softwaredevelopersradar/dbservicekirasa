﻿using System.Runtime.Serialization;

namespace KirasaModelsDBLib
{
    /// <summary>
    /// ИРИ БПЛА Архив
    /// </summary>
    [DataContract]
    [KnownType(typeof(TableUAVBase))]
    [InfoTable(NameTable.TableUAVResArchive)]
    public class TableUAVResArchive : TableUAVBase
    {
    }
}
