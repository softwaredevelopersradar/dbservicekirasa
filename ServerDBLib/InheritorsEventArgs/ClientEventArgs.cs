﻿using System;
using System.Collections.Generic;
using Errors;

namespace InheritorsEventArgs
{
    public class ClientEventArgs : AStandardEventArgs
    {
        public enum ActServer
        {
            Connect,
            Disconnect
        }

        public EnumClientError CurrentError { get; protected set; }

        public override string GetMessage { get; protected set; }

        private readonly Dictionary<string, string> DicMess = new Dictionary<string, string>
        {
            { ActServer.Connect.ToString(),  "Connected" },
            { ActServer.Disconnect.ToString(),"Disconnected" }
        };

        public ClientEventArgs(EnumClientError error)
        {
            GetMessage = $"{DateTime.Now.ToShortTimeString()} ";
            CurrentError = error;
            GetMessage += $"{Errors.ClientError.GetDefenition(CurrentError)} ";
        }

        public ClientEventArgs(EnumClientError error, string errorMess) : this(error)
        {
            GetMessage += errorMess;
        }

        public ClientEventArgs(ActServer actServer)
        {
            GetMessage = $"{DateTime.Now.ToShortTimeString()} ";
            CurrentError = EnumClientError.None;
            GetMessage += "";// DicMess[actServer.ToString()];
        }
        public ClientEventArgs(ActServer actServer, string mess) : this(actServer)
        {
            GetMessage += mess;
        }
    }
}
