﻿using System;
using KirasaModelsDBLib;
using System.Runtime.Serialization;

namespace InheritorsEventArgs
{
    [DataContract]
    [KnownType(typeof(ClassDataCommon))]
    [KnownType(typeof(NameTable))]
    //[KnownType(typeof(EventArgs))]
    public class DataEventArgs : EventArgs
    {
        [DataMember]
        public ClassDataCommon AbstractData { get; protected set; }

        [DataMember]
        public NameTable Name { get; protected set; }

        public DataEventArgs(NameTable nameTable, ClassDataCommon data)
        {
            Name = nameTable;
            AbstractData = data;
        }

    }
}
