﻿using System;
using KirasaModelsDBLib;
using System.Runtime.Serialization;

namespace InheritorsEventArgs
{
    [DataContract]
    [KnownType(typeof(AbstractCommonTable))]
    [KnownType(typeof(NameTable))]
    [KnownType(typeof(NameChangeOperation))]
    public class RecordEventArgs
    {
        [DataMember]
        public AbstractCommonTable AbstractRecord { get; protected set; }

        [DataMember]
        public NameTable Name { get; protected set; }

        [DataMember]
        public NameChangeOperation NameAction { get; protected set; }


        public RecordEventArgs(NameTable nameTable, AbstractCommonTable record, NameChangeOperation action)
        {
            Name = nameTable;
            AbstractRecord = record;
            NameAction = action;
        }
    }
}
