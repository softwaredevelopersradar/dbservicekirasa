﻿using System;
using KirasaModelsDBLib;
using System.Collections.Generic;

namespace InheritorsEventArgs
{
    public class TableEventArgs<T> : EventArgs where T : AbstractCommonTable
    {
        public List<T> Table { get; protected set; }

        public TableEventArgs(ClassDataCommon lData)
        {
            Table = lData.ToList<T>();
        }

        public TableEventArgs(List<T> lData)
        {
            Table = lData;
        }
    }
}
