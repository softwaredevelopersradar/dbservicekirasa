﻿using System;
using KirasaModelsDBLib;
using System.Runtime.Serialization;

namespace InheritorsEventArgs
{
    [DataContract]
    [KnownType(typeof(AStandardEventArgs))]
    [KnownType(typeof(NameTableOperation))]
    [KnownType(typeof(NameChangeOperation))]
    [KnownType(typeof(Errors.EnumDBError))]
    [KnownType(typeof(NameTable))]
    public class OperationTableEventArgs : AStandardEventArgs
    {
        [DataMember]
        public NameTableOperation Operation { get; private set; }

        [DataMember]
        public NameTable TableName { get; protected set; }

        [DataMember]
        public override string GetMessage { get; protected set; }

        [DataMember]
        public Errors.EnumDBError TypeError { get; protected set; }

        public OperationTableEventArgs(NameTableOperation act, Errors.EnumDBError typeError)
        {
            DateTime time = DateTime.Now;
            GetMessage = $"{time.ToLongTimeString()}:{time.Millisecond} ";
            GetMessage += $"Operation: {act.ToString()} ";
            Operation = act;
            TypeError = typeError;
            GetMessage += Errors.DBError.GetDefenition(typeError);
        }

        public OperationTableEventArgs(NameChangeOperation act, Errors.EnumDBError typeError)
        {
            DateTime time = DateTime.Now;
            GetMessage = $"{time.ToLongTimeString()}:{time.Millisecond} ";
            GetMessage += $"Operation: {act.ToString()} ";
            Operation = (NameTableOperation)Enum.Parse(typeof(NameTableOperation), act.ToString());
            TypeError = typeError;
            GetMessage += Errors.DBError.GetDefenition(typeError);
        }

        public OperationTableEventArgs(NameTableOperation act, string message)
        {
            DateTime time = DateTime.Now;
            GetMessage = $"{time.ToLongTimeString()}:{time.Millisecond} ";
            GetMessage += $"Operation: {act.ToString()} ";
            Operation = act;
            GetMessage += message;
            TypeError = Errors.EnumDBError.UnknownError;
        }

        public OperationTableEventArgs(NameChangeOperation act, string message)
        {
            DateTime time = DateTime.Now;
            GetMessage = $"{time.ToLongTimeString()}:{time.Millisecond} ";
            GetMessage += $"Operation: {act.ToString()} ";
            Operation = (NameTableOperation)Enum.Parse(typeof(NameTableOperation), act.ToString());
            GetMessage += message;
            TypeError = Errors.EnumDBError.UnknownError;
        }

        public OperationTableEventArgs(NameTableOperation act, Errors.EnumDBError typeError, string message) : this(act, message)
        {
            TypeError = typeError;
        }

        public OperationTableEventArgs(NameChangeOperation act, Errors.EnumDBError typeError, string message) : this(act, message)
        {
            TypeError = typeError;
        }

    }
}
