﻿using System.Collections.Generic;
using Errors;

namespace InheritorsEventArgs
{
    public class ServerEventArgs : AStandardEventArgs
    {
        public enum ActClient
        {
            Connect,
            Disconnect
        }

        private Dictionary<string, string> dicMess = new Dictionary<string, string>
        {
            { ActClient.Connect.ToString(),  "Connected" },
            { ActClient.Disconnect.ToString(),"Disconnected" }
        };

        public override string GetMessage { get; protected set; }

        private ServerEventArgs(string TimeShortString)
        {
            GetMessage = $"{TimeShortString} ";
        }

        public ServerEventArgs(string TimeShortString, string NameClient, ActClient act) : this(TimeShortString)
        {
            GetMessage += $"{NameClient}: {dicMess[act.ToString()]}";
        }

        public ServerEventArgs(string TimeShortString, EnumServerError error, string Message) : this(TimeShortString)
        {
            GetMessage += $"{ServerErrors.GetDefenition(error)} {Message}";
        }
    }
}
