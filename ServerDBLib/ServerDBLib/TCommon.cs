﻿using System;
using System.Collections.Generic;
using KirasaModelsDBLib;
using OperationsTablesLib;
using InheritorsEventArgs;
using System.ServiceModel;
using System.Threading.Tasks;

namespace ServerDBLib
{
    partial class ServiceDB : IServiceDB
    {
        void ICommonTableOperation.AddRangeRecord(NameTable nameTable, ClassDataCommon data, int IdClient)
        {
            lock (dicOperTables[nameTable])
            {
                try
                {
                    SendMessToHost(IdClient, NameTableOperation.AddRange, nameTable.ToString());

                    if (!clients.ContainsKey(IdClient))
                    {
                        SendErrorOfClient(Errors.EnumServerError.ClientAbsent, "");
                        return;
                    }
                    dicOperTables[nameTable].AddRange(data, IdClient);
                    SendMessToHost(IdClient, NameTableOperation.AddRange, "Ok, count items =" + dicOperTables[nameTable].Load(IdClient).ListRecords.Count);

                }
                catch (InheritorsException.ExceptionLocalDB except)
                {
                    SendError(except, NameTableOperation.AddRange);
                }
                catch (Exception error)
                {
                    SendErrorOfClient(Errors.EnumServerError.UnknownError, error.Message);
                    return;
                }
            }
        }


        void ICommonTableOperation.RemoveRangeRecord(NameTable nameTable, ClassDataCommon data, int IdClient)
        {

            lock (dicOperTables[nameTable])
            {
                try
                {
                    SendMessToHost(IdClient, NameTableOperation.RemoveRange, nameTable.ToString());
                    if (!clients.ContainsKey(IdClient))
                    {
                        SendErrorOfClient(Errors.EnumServerError.ClientAbsent, "");
                        return;
                    }
                    ///Собираю данные о кол-ве записей в таблицах до удаления
                    Dictionary<NameTable, int> keyValuePairs = new Dictionary<NameTable, int>();
                    foreach (var table in dicOperTables)
                    {
                        if (table.Key == nameTable)
                            continue;
                        keyValuePairs.Add(table.Key, table.Value.Load(IdClient).ListRecords.Count);
                    }

                    dicOperTables[nameTable].RemoveRange(data, IdClient);


                    ///И сравниваю данные о кол-ве записей во всех таблицах после удаления
                    ///Это сделано по той причине, что при удалении записей из таблицы, которая имеет связную с ней таблицу, в связной автом. удаляться все зависимые записи

                    foreach (var table in keyValuePairs)
                    {
                        if (dicOperTables[table.Key].Load(IdClient).ListRecords.Count != table.Value)
                            dicOperTables[table.Key].UpDate(IdClient);
                    }
                    
                    SendMessToHost(IdClient, NameTableOperation.RemoveRange, "Ok, count items =" + dicOperTables[nameTable].Load(IdClient).ListRecords.Count);

                }
                catch (InheritorsException.ExceptionLocalDB except)
                {
                    SendError(except, NameTableOperation.RemoveRange);
                }
                catch (Exception error)
                {
                    SendErrorOfClient(Errors.EnumServerError.UnknownError, error.Message);
                    return;
                }
            }
        }


        void ICommonTableOperation.ChangeRecord(NameTable nameTable, NameChangeOperation nameOperation, AbstractCommonTable record, int IdClient)
        {
            lock (dicOperTables[nameTable])
            {
                try
                {
                    if (nameTable == NameTable.TableАeroscope)
                    {
                        SendMessToHost(IdClient, nameOperation, nameTable.ToString() + ", SerialNum=" + (record as TableAeroscope).SerialNumber);
                    }
                    else if (nameTable == NameTable.TableАeroscopeTrajectory)
                    {
                        SendMessToHost(IdClient, nameOperation, nameTable.ToString() + ", Id=" + record.Id + ", Num=" + (record as TableAeroscopeTrajectory).Num + ", SerialNum=" + (record as TableAeroscopeTrajectory).SerialNumber);
                    }
                    else
                    {
                        SendMessToHost(IdClient, nameOperation, nameTable.ToString());
                    }
                    //SendMessToHost(IdClient, nameOperation, nameTable.ToString());
                    if (!clients.ContainsKey(IdClient))
                    {
                        SendErrorOfClient(Errors.EnumServerError.ClientAbsent, "");
                        return;
                    }

                    switch (nameOperation)
                    {
                        case NameChangeOperation.Add:
                            dicOperTables[nameTable].Add(record, IdClient);
                            break;
                        case NameChangeOperation.Change:
                            dicOperTables[nameTable].Change(record, IdClient);
                            break;
                        case NameChangeOperation.Delete:
                            
                            Action actionDelete = () => dicOperTables[nameTable].Delete(record, IdClient);

                            if (dicOperTables[nameTable].IsTemp)
                            {
                                actionDelete();
                                break;
                            }
                            //хранит кол-во записей
                            Dictionary<NameTable, int> keyValuePairs = new Dictionary<NameTable, int>();

                            foreach (var table in dicOperTables)
                            {
                                if (table.Key == nameTable)
                                    continue;
                                keyValuePairs.Add(table.Key, table.Value.Load(IdClient).ListRecords.Count);
                            }

                            actionDelete();
                            //if (nameTable == NameTable.TableMission) break;

                            foreach (var table in keyValuePairs)
                            {
                                if (dicOperTables[table.Key].Load(IdClient).ListRecords.Count != table.Value)
                                    dicOperTables[table.Key].UpDate(IdClient);
                            }

                            break;
                    }
                    SendMessToHost(IdClient, nameOperation, "Ok, count items =" + dicOperTables[nameTable].Load(IdClient).ListRecords.Count);

                }
                catch (InheritorsException.ExceptionLocalDB except)
                {
                    SendError(except, nameOperation);
                }
                catch (Exception error)
                {
                    SendErrorOfClient(Errors.EnumServerError.UnknownError, error.Message);
                    return;
                }
            }
        }


        void ICommonTableOperation.ClearTable(NameTable nameTable, int IdClient)
        {
            lock (dicOperTables[nameTable])
            {
                try
                {
                    SendMessToHost(IdClient, NameTableOperation.Clear, nameTable.ToString());
                    if (!clients.ContainsKey(IdClient))
                    {
                        SendErrorOfClient(Errors.EnumServerError.ClientAbsent, "");
                        return;
                    }

                    Dictionary<NameTable, int> keyValuePairs = new Dictionary<NameTable, int>();

                    foreach (var table in dicOperTables)
                    {
                        if (table.Key == nameTable)
                            continue;
                        keyValuePairs.Add(table.Key, table.Value.Load(IdClient).ListRecords.Count);
                    }
                    dicOperTables[nameTable].Clear(IdClient);

                    foreach (var table in keyValuePairs)
                    {
                        if (dicOperTables[table.Key].Load(IdClient).ListRecords.Count != table.Value)
                            dicOperTables[table.Key].UpDate(IdClient);
                    }
                    
                    SendMessToHost(IdClient, NameTableOperation.Clear, "Ok, count items =" + dicOperTables[nameTable].Load(IdClient).ListRecords.Count);
                }
                catch (InheritorsException.ExceptionLocalDB except)
                {
                    SendError(except, NameTableOperation.Clear);
                }
                catch (Exception error)
                {
                    SendErrorOfClient(Errors.EnumServerError.UnknownError, error.Message);
                    return;
                }
            }
        }

        ClassDataCommon ICommonTableOperation.LoadData(NameTable nameTable, int IdClient)
        {
            try
            {
                SendMessToHost(IdClient, NameTableOperation.Load, nameTable.ToString());
                if (!clients.ContainsKey(IdClient))
                {
                    SendErrorOfClient(Errors.EnumServerError.ClientAbsent, "");
                    return null;
                }
                return dicOperTables[nameTable].Load(IdClient);
            }
            catch (InheritorsException.ExceptionLocalDB except)
            {
                SendErrorToHost(new OperationTableEventArgs(NameTableOperation.Load, except.Error, except.Message));

                throw new FaultException<InheritorsException.ExceptionWCF>(new InheritorsException.ExceptionWCF(except));
            }
            catch (Exception error)
            {
                SendErrorToHost(new OperationTableEventArgs(NameTableOperation.Load, error.Message));

                throw new FaultException<InheritorsException.ExceptionWCF>(new InheritorsException.ExceptionWCF(error.Message));

                //SendErrorOfClient(ServerEventArgs.Error.UnknownError, error.Message);
                //return null;
            }
        }

    }
}
