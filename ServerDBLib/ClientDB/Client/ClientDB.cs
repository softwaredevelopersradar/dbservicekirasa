﻿using System;
using System.ServiceModel;
using ClientDataBase.ServiceDB;
using KirasaModelsDBLib;
using InheritorsEventArgs;
using System.Threading.Tasks;
using OperationTableEventArgs = InheritorsEventArgs.OperationTableEventArgs;


namespace ClientDataBase
{
    [CallbackBehavior(ConcurrencyMode = ConcurrencyMode.Multiple, UseSynchronizationContext = false)]
    public partial class ClientDB //: IServiceDBCallback
    {
        public ClientDB(string NameApplication, string EndPointAddress)
        {
            name = NameApplication;
            if (ValidEndPoint(EndPointAddress))
                endpointAddress = "net.tcp://" + EndPointAddress + "/";
            else
            {
                throw new ExceptionClient(Errors.EnumClientError.IncorrectEndpoint);
            }
        }

        ~ClientDB()
        {
            try { Disconnect(); }
            catch { }
        }

        public bool IsConnected()
        {
            if (ClientServiceDB == null)
                return false;
            try
            {
                return ClientServiceDB.Ping(ID);
            }
            catch (Exception except)
            {
                OnErrorDataBase(this, new OperationTableEventArgs(NameTableOperation.Update, except.Message));
                return false;
            }
        }

        public string NameEndPoint
        {
            get { return endpointAddress; }
        }

        public async void ConnectAsync()
        {
            try
            {
                if (!IsConnected())
                {
                    try
                    { ClientServiceDB = new ServiceDBClient(
                        new System.ServiceModel.InstanceContext(this), "NetTcpBinding_IServiceDB"); }
                    catch (Exception e)
                    { Console.WriteLine(e.Message); }
                    ClientServiceDB.Endpoint.Binding = GetBinding();
                    ClientServiceDB.Endpoint.Address = new EndpointAddress(endpointAddress);
                    ID = await ClientServiceDB.ConnectAsync(name);

                    ClassTable.Id = ID;
                    ClassTable.ClientServiceDB = ClientServiceDB;

                    OnConnect(this, new ClientEventArgs(ClientEventArgs.ActServer.Connect, $"Id = {ID.ToString()}"));
                }
            }
            catch (Exception error)
            {
                OnDisconnect(this, new ClientEventArgs(ClientEventArgs.ActServer.Disconnect, error.Message));
                //throw new Exceptions.ExceptionClient(error.Message);
            }
        }

        public void Connect()
        {
            try
            {
                if (IsConnected())
                {
                    OnConnect(this, new ClientEventArgs(ClientEventArgs.ActServer.Connect));
                    return;
                }

                ClientServiceDB = new ServiceDBClient(new InstanceContext(this), "NetTcpBinding_IServiceDB");
                ClientServiceDB.Endpoint.Binding = GetBinding();
                ClientServiceDB.Endpoint.Address = new EndpointAddress(endpointAddress);
                ID = ClientServiceDB.Connect(name);

                ClassTable.Id = ID;
                ClassTable.ClientServiceDB = ClientServiceDB;

                OnConnect(this, new ClientEventArgs(ClientEventArgs.ActServer.Connect, $"Id = {ID.ToString()}"));

            }
            catch (Exception error)
            {
                OnDisconnect(this, new ClientEventArgs(ClientEventArgs.ActServer.Disconnect, error.Message));
                //throw new Exceptions.ExceptionClient(error.Message);
            }
        }

        private NetTcpBinding GetBinding()
        {
            NetTcpBinding binding = new NetTcpBinding
            {
                Name = "NetTcpBinding_IServiceDB"
            };
            binding.ReliableSession.Enabled = true;
            binding.ReliableSession.InactivityTimeout = TimeSpan.MaxValue;
            binding.ReceiveTimeout = TimeSpan.MaxValue;
            binding.CloseTimeout = TimeSpan.MaxValue;
            binding.SendTimeout = new TimeSpan(0, 0, 50);
            binding.OpenTimeout = new TimeSpan(0, 0, 50);
            binding.Security.Mode = SecurityMode.None;
            binding.MaxReceivedMessageSize = Int32.MaxValue;
            binding.MaxBufferSize = Int32.MaxValue;
            binding.MaxBufferPoolSize = long.MaxValue;
            return binding;
        }

        public void Disconnect()
        {
            try
            {
                if (ClientServiceDB != null)
                {
                    ClientServiceDB.Disconnect(ID);
                    Abort();
                }
            }
            catch (Exception error)
            {
                throw new ExceptionClient(error.Message);
            }
        }

        public async void DisconnectAsync()
        {
            try
            {
                if (ClientServiceDB != null)
                {
                    await ClientServiceDB.DisconnectAsync(ID);
                    Abort();
                }
            }
            catch (Exception error)
            {
                throw new ExceptionClient(error.Message);
            }
        }
    }
}
