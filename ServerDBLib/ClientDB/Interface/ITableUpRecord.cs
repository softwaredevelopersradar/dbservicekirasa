﻿using System;

namespace ClientDataBase
{
    public interface ITableUpRecord<T> where T : KirasaModelsDBLib.AbstractCommonTable
    {
        event EventHandler<T> OnAddRecord;

        event EventHandler<T> OnDeleteRecord;

        event EventHandler<T> OnChangeRecord;
    }
}
