﻿using InheritorsEventArgs;
using System;

namespace ClientDataBase
{
    public interface ITableUpdate<T> where T : KirasaModelsDBLib.AbstractCommonTable
    {
        event EventHandler<TableEventArgs<T>> OnUpTable;
    }
}
