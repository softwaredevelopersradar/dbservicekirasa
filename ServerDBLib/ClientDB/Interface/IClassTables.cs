﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace ClientDataBase
{
    public interface IClassTables
    {
        void Add(object obj);

        void AddRange(object rangeObj);

        void RemoveRange(object rangeObj);

        void Delete(object obj);

        void Clear();

        void Change(object obj);

        List<T> Load<T>() where T : KirasaModelsDBLib.AbstractCommonTable;

        Task<List<T>> LoadAsync<T>() where T : KirasaModelsDBLib.AbstractCommonTable;


        Task AddAsync(object obj);

        Task AddRangeAsync(object rangeObj);

        Task RemoveRangeAsync(object rangeObj);

        Task DeleteAsync(object obj);

        Task ClearAsync();

        Task ChangeAsync(object obj);
    }
}
