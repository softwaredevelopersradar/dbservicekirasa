﻿using InheritorsEventArgs;
using System;

namespace ClientDataBase
{
    public interface ITableAddRange<T> where T : KirasaModelsDBLib.AbstractCommonTable
    {
        event EventHandler<TableEventArgs<T>> OnAddRange;
    }
}
