﻿using Errors;

namespace ClientDataBase
{
    public interface IExceptionClient
    {
        EnumClientError Error { get; }

        string Message { get; }
    }
}
