﻿using System;
using Errors;

namespace ClientDataBase
{
    public class ExceptionDatabase : Exception, InheritorsException.IExceptionDb
    {
        public EnumDBError Error { get; protected set; }

        public ExceptionDatabase(EnumDBError error) : base(DBError.GetDefenition(error))
        {
            Error = error;
        }

        public ExceptionDatabase(string message) : base(message)
        {
            Error = EnumDBError.UnknownError;
        }

        public ExceptionDatabase(InheritorsException.IExceptionDb exception) : base(exception.Message)
        {
            Error = exception.Error;
        }
    }
}
