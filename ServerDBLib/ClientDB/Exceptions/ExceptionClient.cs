﻿using System;
using Errors;

namespace ClientDataBase
{
    public class ExceptionClient : Exception, IExceptionClient
    {
        public EnumClientError Error { get; protected set; }

        public ExceptionClient(EnumClientError error) : base(ClientError.GetDefenition(error))
        {
            Error = error;
        }

        public ExceptionClient(string message) : base(message)
        {
            Error = EnumClientError.UnknownError;
        }

        public ExceptionClient(IExceptionClient exception) : base(exception.Message)
        {
            Error = exception.Error;
        }
    }
}
