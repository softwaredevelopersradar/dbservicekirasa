﻿using System;
using ClientDataBase.ServiceDB;

namespace ClientDataBase
{
    internal class ClassTable : IDisposable
    {
        public static ServiceDBClient ClientServiceDB { get; internal set; } = null;

        public static int Id { get; internal set; } = 0;

        public ClassTable(ServiceDBClient clientServiceDB, int ID)
        {
            if (ClientServiceDB != null)
                return;

            ClientServiceDB = clientServiceDB;
            Id = ID;
        }

        public ClassTable()
        { }

        public void Dispose()
        {
            if (ClientServiceDB == null)
                return;

            ClientServiceDB.Abort();
            ClientServiceDB = null;
        }
    }
}
