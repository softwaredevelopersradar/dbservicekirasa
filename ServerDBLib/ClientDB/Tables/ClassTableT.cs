﻿using System;
using System.Collections.Generic;
using ClientDataBase.ServiceDB;
using KirasaModelsDBLib;
using InheritorsEventArgs;
using System.ServiceModel;
using System.Threading.Tasks;

namespace ClientDataBase
{
    internal class ClassTable<T> : ClassTable, IClassTables, IClickUpData, IClickUpRecord, IClickUpAddRange, ITableUpRecord<T>, ITableAddRange<T>, ITableUpdate<T> where T : AbstractCommonTable
    {
        protected NameTable Name;
        #region ITableAddRange

        public event EventHandler<TableEventArgs<T>> OnAddRange = (object obj, TableEventArgs<T> eventArs) => { };

        public void ClickUpAddRange(ClassDataCommon dataCommon)
        {
            OnAddRange(this, new TableEventArgs<T>(dataCommon));
        }
        #endregion

        #region UpFullData

        public event EventHandler<TableEventArgs<T>> OnUpTable = (object obj, TableEventArgs<T> eventArs) => { };

        public void ClickUpTable(ClassDataCommon data)
        {
            OnUpTable(this, new TableEventArgs<T>(data));
        }

        #endregion

        #region UpRecord

        public event EventHandler<T> OnAddRecord = (obj, eventArgs) => { };

        public event EventHandler<T> OnDeleteRecord = (obj, eventArgs) => { };

        public event EventHandler<T> OnChangeRecord = (obj, eventArgs) => { };


        public void ClickUpRecord(InheritorsEventArgs.RecordEventArgs eventArgs)
        {
            switch (eventArgs.NameAction)
            {
                case NameChangeOperation.Add:
                    OnAddRecord(this, eventArgs.AbstractRecord as T);
                    break;
                case NameChangeOperation.Change:
                    OnChangeRecord(this, eventArgs.AbstractRecord as T);
                    break;
                case NameChangeOperation.Delete:
                    OnDeleteRecord(this, eventArgs.AbstractRecord as T);
                    break;
            }
        }

        #endregion

        public ClassTable() : base()
        {
            object[] attrs = typeof(T).GetCustomAttributes(typeof(InfoTableAttribute), false);
            foreach (InfoTableAttribute info in attrs)
            {
                Name = info.Name;
            }
        }

        public ClassTable(ref ServiceDBClient clientServiceDB, int id) : base(clientServiceDB, id)
        {
            object[] attrs = typeof(T).GetCustomAttributes(typeof(InfoTableAttribute), false);
            foreach (InfoTableAttribute info in attrs)
            {
                Name = info.Name;
            }
        }

        protected bool ValidConnection()
        {
            if (ClientServiceDB == null)
            {
                //добавить ф-цию на стороне сервера Ping()
                // ErrorCallbackClient(this, Errors.EnumClientError.NoConnection, "");
                return false;
            }
            return true;
        }

        public void Add(object obj)
        {
            try
            {
                if (!ValidConnection())
                {
                    throw new ExceptionClient(Errors.EnumClientError.NoConnection);
                }
                AbstractCommonTable record = obj as AbstractCommonTable;
                ClientServiceDB.ChangeRecord(Name, NameChangeOperation.Add, record, Id);
                return;
            }
            catch (ExceptionClient except)
            {
                throw new ExceptionClient(except);
            }
            catch (Exception error)
            {
                throw new ExceptionClient(error.Message);
            }
        }

        public async Task AddAsync(object obj)
        {
            try
            {
                if (!ValidConnection())
                {
                    throw new ExceptionClient(Errors.EnumClientError.NoConnection);
                }
                AbstractCommonTable record = obj as AbstractCommonTable;
                await ClientServiceDB.ChangeRecordAsync(Name, NameChangeOperation.Add, record, Id);
                return;
            }
            catch (ExceptionClient except)
            {
                throw new ExceptionClient(except);
            }
            catch (Exception error)
            {
                throw new ExceptionClient(error.Message);
            }
        }

        public void AddRange(object rangeObj)
        {
            try
            {
                if (!ValidConnection())
                {
                    throw new ExceptionClient(Errors.EnumClientError.NoConnection);
                }
                if (rangeObj is List<T>)
                {
                    ClassDataCommon data = ClassDataCommon.ConvertToListAbstractCommonTable(rangeObj as List<T>);
                    ClientServiceDB.AddRangeRecord(Name, data, Id);
                    return;
                }
                throw new ExceptionClient("AddRange: Mismatch with expected type!");
            }
            catch (ExceptionClient except)
            {
                throw new ExceptionClient(except);
            }
            catch (Exception error)
            {
                throw new ExceptionClient(error.Message);
            }
        }

        public async Task AddRangeAsync(object rangeObj)
        {
            try
            {
                if (!ValidConnection())
                {
                    throw new ExceptionClient(Errors.EnumClientError.NoConnection);
                }
                if (rangeObj is List<T>)
                {
                    ClassDataCommon data = ClassDataCommon.ConvertToListAbstractCommonTable(rangeObj as List<T>);
                    await ClientServiceDB.AddRangeRecordAsync(Name, data, Id);
                    return;
                }
                throw new ExceptionClient("AddRange: Mismatch with expected type!");
            }
            catch (ExceptionClient except)
            {
                throw new ExceptionClient(except);
            }
            catch (Exception error)
            {
                throw new ExceptionClient(error.Message);
            }
        }

        public void RemoveRange(object rangeObj)
        {
            try
            {
                if (!ValidConnection())
                {
                    throw new ExceptionClient(Errors.EnumClientError.NoConnection);
                }
                if (rangeObj is List<T>)
                {
                    ClassDataCommon data = ClassDataCommon.ConvertToListAbstractCommonTable(rangeObj as List<T>);
                    ClientServiceDB.RemoveRangeRecord(Name, data, Id);
                    return;
                }
                throw new ExceptionClient("RemoveRange: Mismatch with expected type!");
            }
            catch (ExceptionClient except)
            {
                throw new ExceptionClient(except);
            }
            catch (Exception error)
            {
                throw new ExceptionClient(error.Message);
            }
        }

        public async Task RemoveRangeAsync(object rangeObj)
        {
            try
            {
                if (!ValidConnection())
                {
                    throw new ExceptionClient(Errors.EnumClientError.NoConnection);
                }
                if (rangeObj is List<T>)
                {
                    ClassDataCommon data = ClassDataCommon.ConvertToListAbstractCommonTable(rangeObj as List<T>);
                    await ClientServiceDB.RemoveRangeRecordAsync(Name, data, Id);
                    return;
                }
                throw new ExceptionClient("RemoveRange: Mismatch with expected type!");
            }
            catch (ExceptionClient except)
            {
                throw new ExceptionClient(except);
            }
            catch (Exception error)
            {
                throw new ExceptionClient(error.Message);
            }
        }

        public void Change(object obj)
        {
            try
            {
                if (!ValidConnection())
                {
                    throw new ExceptionClient(Errors.EnumClientError.NoConnection);
                }
                AbstractCommonTable record = obj as AbstractCommonTable;
                ClientServiceDB.ChangeRecord(Name, NameChangeOperation.Change, record, Id);
                return;
            }
            catch (ExceptionClient except)
            {
                throw new ExceptionClient(except);
            }
            catch (Exception error)
            {
                throw new ExceptionClient(error.Message);
            }
        }

        public async Task ChangeAsync(object obj)
        {
            try
            {
                if (!ValidConnection())
                {
                    throw new ExceptionClient(Errors.EnumClientError.NoConnection);
                }
                AbstractCommonTable record = obj as AbstractCommonTable;
                await ClientServiceDB.ChangeRecordAsync(Name, NameChangeOperation.Change, record, Id);
                return;
            }
            catch (ExceptionClient except)
            {
                throw new ExceptionClient(except);
            }
            catch (Exception error)
            {
                throw new ExceptionClient(error.Message);
            }
        }

        public void Clear()
        {
            try
            {
                if (!ValidConnection())
                {
                    throw new ExceptionClient(Errors.EnumClientError.NoConnection);
                }
                ClientServiceDB.ClearTable(Name, Id);
                return;
            }
            catch (ExceptionClient except)
            {
                throw new ExceptionClient(except);
            }
            catch (Exception error)
            {
                throw new ExceptionClient(error.Message);
            }
        }

        public async Task ClearAsync()
        {
            try
            {
                if (!ValidConnection())
                {
                    throw new ExceptionClient(Errors.EnumClientError.NoConnection);
                }
                await ClientServiceDB.ClearTableAsync(Name, Id);
                return;
            }
            catch (ExceptionClient except)
            {
                throw new ExceptionClient(except);
            }
            catch (Exception error)
            {
                throw new ExceptionClient(error.Message);
            }
        }

        public void Delete(object obj)
        {
            try
            {
                if (!ValidConnection())
                {
                    throw new ExceptionClient(Errors.EnumClientError.NoConnection);
                }
                var record = obj as AbstractCommonTable;
                ClientServiceDB.ChangeRecord(Name, NameChangeOperation.Delete, record, Id);
                return;
            }
            catch (ExceptionClient except)
            {
                throw new ExceptionClient(except);
            }
            catch (Exception error)
            {
                throw new ExceptionClient(error.Message);
            }
        }

        public async Task DeleteAsync(object obj)
        {
            try
            {
                if (!ValidConnection())
                {
                    throw new ExceptionClient(Errors.EnumClientError.NoConnection);
                }
                var record = obj as AbstractCommonTable;
                await ClientServiceDB.ChangeRecordAsync(Name, NameChangeOperation.Delete, record, Id);
                return;
            }
            catch (ExceptionClient except)
            {
                throw new ExceptionClient(except);
            }
            catch (Exception error)
            {
                throw new ExceptionClient(error.Message);
            }
        }

        public List<V> Load<V>() where V : AbstractCommonTable
        {
            ClassDataCommon Data = new ClassDataCommon();
            try
            {
                if (!ValidConnection())
                {
                    throw new ExceptionClient(Errors.EnumClientError.NoConnection);
                }
                Data = ClientServiceDB.LoadData(Name, Id);
                return Data.ToList<V>();
            }
            catch (ExceptionClient except)
            {
                throw new ExceptionClient(except);
            }
            catch (FaultException<InheritorsException.ExceptionWCF> except)
            {
                throw new ExceptionDatabase(except.Detail);
            }
            catch (Exception error)
            {
                throw new ExceptionClient(error.Message);
            }
        }

        public async Task<List<V>> LoadAsync<V>() where V : AbstractCommonTable
        {
            try
            {
                if (!ValidConnection())
                {
                    throw new ExceptionClient(Errors.EnumClientError.NoConnection);
                }
                var result = await ClientServiceDB.LoadDataAsync(Name, Id);
                return result.ToList<V>();
            }
            catch (ExceptionClient except)
            {
                throw new ExceptionClient(except);
            }
            catch (FaultException<InheritorsException.ExceptionWCF> except)
            {
                throw new ExceptionDatabase(except.Detail);
            }
            catch (Exception error)
            {
                throw new ExceptionClient(error.Message);
            }
        }

    }
}
