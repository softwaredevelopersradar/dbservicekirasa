﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Errors
{
    [DataContract]
    public class DBError
    {
        public static string GetDefenition(EnumDBError error)
        {
            try { return DicError[error]; }
            catch (Exception) { return ""; }
        }

        [DataMember]
        private static readonly Dictionary<EnumDBError, string> DicError = new Dictionary<EnumDBError, string>
        {
            { EnumDBError.UnknownError, "Error: "},
            { EnumDBError.RecordExist, "Error: The table already has the similar record! "},
            { EnumDBError.RecordNotFound, "Error: The record not found! "},
            { EnumDBError.None, ""},
            { EnumDBError.SuchAeroscopeAbsent, "Error: There is no such SerialNum in Aeroscope"},
            { EnumDBError.SuchUAVAbsent, "Error: There is no such UAV"}
        };
    }
}
