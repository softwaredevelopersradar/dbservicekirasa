﻿using System;
using KirasaModelsDBLib;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace OperationsTablesLib
{
    public class OperationTableUAVArchive : OperationTableDb<TableUAVResArchive>
    {
        public override void AddRange(ClassDataCommon data, int idClient)
        {
            try
            {
                List<TableUAVResArchive> AddRange = new List<TableUAVResArchive>();
                lock (DataBase)
                {
                    DbSet<TableUAVResArchive> Table = DataBase.GetTable<TableUAVResArchive>(Name);
                    foreach (var record in data.ListRecords)
                    {
                        var rec = Table.Find(record.GetKey());
                        if (rec != null)
                        {
                            rec.Update(record);
                            Table.Update(rec);
                        }
                        else
                        {
                            Table.Add(record as TableUAVResArchive);
                            AddRange.Add(record as TableUAVResArchive);
                        }
                    }
                    DataBase.SaveChanges();
                }
                UpDate(idClient);
                //SendRange(this, new DataEventArgs(Name, ClassDataCommon.ConvertToListAbstractCommonTable(AddRange)));
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }
    }
}
