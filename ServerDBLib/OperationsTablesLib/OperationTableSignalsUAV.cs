﻿using System;
using KirasaModelsDBLib;
using Microsoft.EntityFrameworkCore;
using Errors;

namespace OperationsTablesLib
{
    public class OperationTableSignalsUAV : OperationTableDb<TableSignalsUAV>
    {
        public override void Delete(AbstractCommonTable record, int idClient)
        {
            try
            {
                lock (DataBase)
                {
                    DbSet<TableSignalsUAV> Table = DataBase.GetTable<TableSignalsUAV>(Name);

                    TableSignalsUAV rec = Table.Find(record.GetKey());
                    if (rec != null)
                    {
                        Table.Remove(rec);
                        DataBase.SaveChanges();
                    }
                    UpRecord(idClient, record, NameChangeOperation.Delete);
                    return;
                }
                throw new InheritorsException.ExceptionLocalDB(idClient, EnumDBError.RecordNotFound);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }
    }
}
