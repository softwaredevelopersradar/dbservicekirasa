﻿using System;
using System.Linq;
using KirasaModelsDBLib;

namespace OperationsTablesLib
{
   
    public class OperationTempUAV : OperationTempTable<TableUAVRes>
    {
        public override void RemoveRange(ClassDataCommon data, int idClient)
        {
            try
            {
                foreach (var rec in data.ListRecords)
                {
                    int key = (int)rec.GetKey().FirstOrDefault();
                    if (TempTable.ContainsKey(key))
                    {
                        TempTable.Remove(key);
                        ClearTrajectory(key);
                    }
                }
                UpDate(idClient);

            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }


        public override void Clear(int idClient)
        {
            try
            {
                foreach (var record in TempTable)
                {
                    ClearTrajectory(record.Key);
                }

                TempTable.Clear();
                UpDate(idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }


        public override void Delete(AbstractCommonTable record, int idClient)
        {
            try
            {
                int key = (int)record.GetKey().FirstOrDefault();
                if (!TempTable.ContainsKey(key))
                {
                    throw new InheritorsException.ExceptionLocalDB(idClient, Errors.EnumDBError.RecordNotFound);
                }
                
                TempTable.Remove(key);
                UpRecord(idClient, record, NameChangeOperation.Delete);
                //UpDate(idClient);

                ClearTrajectory(key);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }


        private void ClearTrajectory(int key)
        {
            var listRemove = OperationTempUAVTraj.TempTable.Where(t => t.Value.TableUAVResId == key).ToList();
            foreach (var rec in listRemove)
            {
                OperationTempUAVTraj.TempTable.Remove(rec);
            }
        }
    }
}
