﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KirasaModelsDBLib;
using InheritorsEventArgs;
using Microsoft.EntityFrameworkCore;
using Errors;
using System.Collections;

namespace OperationsTablesLib
{
    public class OperationTableReceivingPoint<T> : OperationTableDb<T> where T : TableReceivingPoint
    {
        public override void Add(AbstractCommonTable record, int idClient)
        {
            try
            {
                lock (DataBase)
                {
                    var Table = DataBase.GetTable<T>(Name);
                    if (Table.Find(record.GetKey()) != null)
                    {
                        throw new InheritorsException.ExceptionLocalDB(idClient, EnumDBError.RecordExist);
                    }

                    if ((record as T).IsCetral)
                    {
                        var OtherRecs = Table.Where(rec => (rec as T).IsCetral == true);
                        foreach (var otherRecs in OtherRecs)
                            (otherRecs as T).IsCetral = false;
                    }
                    Table.Add(record as T);
                    DataBase.SaveChanges();
                }
                UpDate(idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                if (exception.InnerException != null)
                    throw exception.InnerException;
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public override void Change(AbstractCommonTable record, int idClient)
        {
            try
            {
                bool hasChanged = false;

                lock (DataBase)
                {
                    DbSet<T> Table = DataBase.GetTable<T>(Name);

                    T rec = Table.Find(record.GetKey());
                    if (rec == null)
                        throw new InheritorsException.ExceptionLocalDB(idClient, EnumDBError.RecordNotFound);

                    hasChanged = (record as T).IsCetral && !rec.IsCetral;

                    if (hasChanged)
                    {
                        var OtherRecs = Table.Where(t => (t as T).IsCetral == true);
                        foreach (var otherRecs in OtherRecs)
                            (otherRecs as T).IsCetral = false;
                    }
                    (rec as AbstractCommonTable).Update(record);
                    Table.Update(rec);
                    DataBase.SaveChanges();
                }
                UpDate(idClient);
                return;
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }
    }
}
