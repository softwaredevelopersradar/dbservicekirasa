﻿using System.ServiceModel;
using KirasaModelsDBLib;
using InheritorsEventArgs;
using InheritorsException;

namespace OperationsTablesLib
{
    using System.Runtime.Serialization;

    [ServiceContract(CallbackContract = typeof(ICommonCallback))]
    #region KnownType
    [ServiceKnownType(typeof(ClassDataCommon))]
    [ServiceKnownType(typeof(AbstractCommonTable))]
    [ServiceKnownType(typeof(TableFreqKnown))]
    [ServiceKnownType(typeof(TableFreqRangesRecon))]
    [ServiceKnownType(typeof(TableFreqRanges))]
    [ServiceKnownType(typeof(TableLocalPoints))]
    [ServiceKnownType(typeof(TableRemotePoints))]
    [ServiceKnownType(typeof(TableReceivingPoint))]
    [ServiceKnownType(typeof(TableUAVRes))]
    [ServiceKnownType(typeof(TableUAVBase))]
    [ServiceKnownType(typeof(TableUAVResArchive))]
    [ServiceKnownType(typeof(TableUAVTrajectory))]
    [ServiceKnownType(typeof(TableTrajectory))]
    [ServiceKnownType(typeof(TableUAVTrajectoryArchive))]
    [ServiceKnownType(typeof(TableAeroscope))]
    [ServiceKnownType(typeof(TableAeroscopeTrajectory))]
    [ServiceKnownType(typeof(GlobalProperties))]
    [ServiceKnownType(typeof(TableSignalsUAV))]
    [ServiceKnownType(typeof(TableOtherPoints))]
    [ServiceKnownType(typeof(TableSecurityGuard))]


    [ServiceKnownType(typeof(NameTable))]
    [ServiceKnownType(typeof(Languages))]
    [ServiceKnownType(typeof(NameChangeOperation))]
    [ServiceKnownType(typeof(NameTableOperation))]
    [ServiceKnownType(typeof(TypeUAVRes))]
    [ServiceKnownType(typeof(TypeSystem))]
    [ServiceKnownType(typeof(TypeL))]
    [ServiceKnownType(typeof(TypeM))]
    [ServiceKnownType(typeof(TypeOtherPoints))]
    [ServiceKnownType(typeof(SamplingFrequency))]

    [ServiceKnownType(typeof(InheritorsException.ExceptionWCF))]
    //[ServiceKnownType(typeof(Errors.DBError))]
    //[ServiceKnownType(typeof(InheritorsEventArgs.DataEventArgs))]
    //[ServiceKnownType(typeof(InheritorsEventArgs.AStandardEventArgs))]
    //[ServiceKnownType(typeof(InheritorsEventArgs.OperationTableEventArgs))]
    #endregion
    public interface ICommonTableOperation
    {
        [OperationContract(IsOneWay = true)]
        // [FaultContract(typeof(ExceptionWCF))]
        void ChangeRecord(NameTable nameTable, NameChangeOperation nameAction, AbstractCommonTable record, int IdClient);

        [OperationContract(IsOneWay = true)]
        //[FaultContract(typeof(ExceptionWCF))]
        void AddRangeRecord(NameTable nameTable, ClassDataCommon rangeRecord, int IdClient);

        [OperationContract(IsOneWay = true)]
        //[FaultContract(typeof(ExceptionWCF))]
        void RemoveRangeRecord(NameTable nameTable, ClassDataCommon rangeRecord, int IdClient);

        [OperationContract(IsOneWay = true)]
        //[FaultContract(typeof(ExceptionWCF))]
        void ClearTable(NameTable nameTable, int IdClient);

        [OperationContract]
        [FaultContract(typeof(ExceptionWCF))]
        [ServiceKnownType(typeof(InheritorsException.ExceptionWCF))]
        ///<summary>
        ///Загрузка данных для выбранной обстановки
        ///</summary>
        ClassDataCommon LoadData(NameTable nameTable, int IdClient);




        //[OperationContract(IsOneWay = true)]
        /////<summary>
        /////Очистка данных по фильтру
        ///// </summary>
        //void ClearTableByFilter(NameTable nameTable, int IdFilter, int IdClient);
    }

    public interface ICommonCallback
    {
        /// <summary>
        /// Получены данные из БД
        /// </summary>
        /// <param name="lData">Данные из таблицы</param>
        [OperationContract(IsOneWay = true)]//(IsOneWay = true)]
                                            //[FaultContract(typeof(ExceptionWCF))]
        void DataCallback(DataEventArgs dataEventArgs);

        [OperationContract(IsOneWay = true)]
        void RecordCallBack(RecordEventArgs recordEventArgs);

        [OperationContract(IsOneWay = true)]
        void RangeCallBack(DataEventArgs dataEventArgs);
    }
}
