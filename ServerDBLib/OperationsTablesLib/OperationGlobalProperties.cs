﻿using KirasaModelsDBLib;
using Microsoft.EntityFrameworkCore;

namespace OperationsTablesLib
{
    public class OperationGlobalProperties : OperationTableDb<GlobalProperties>
    {
        public override void Add(AbstractCommonTable record, int idClient)
        {
            try
            {
                GlobalProperties recFind;
                lock (DataBase)
                {
                    DbSet<GlobalProperties> Table = DataBase.GetTable<GlobalProperties>(Name);
                    recFind = Table.Find(record.GetKey());
                }
                if (recFind != null)
                    base.Change(record, idClient);
                else
                    base.Add(record, idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
        }
    }
}
