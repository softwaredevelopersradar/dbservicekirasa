﻿using System;
using System.Collections.Generic;
using System.Linq;
using KirasaModelsDBLib;
using InheritorsEventArgs;

namespace OperationsTablesLib
{
    public class OperationTempAeroscope : Operation, ITableAction //OperationTempTable<TableAeroscope>// OperationTempTable<TempADSB>
    {
        internal static Dictionary<string, TableAeroscope> TempTable = new Dictionary<string, TableAeroscope>();
        object locker = new object();

        public OperationTempAeroscope() : base()
        {
            IsTemp = true;
            Name = NameTable.TableАeroscope;
        }


        public void Add(AbstractCommonTable record, int idClient)
        {
            try
            {
                lock (locker)
                {
                    if (TempTable.ContainsKey((string)record.GetKey().FirstOrDefault()))
                    {
                        throw new InheritorsException.ExceptionLocalDB(idClient, Errors.EnumDBError.RecordExist);
                    }

                    TempTable.Add((string)record.GetKey().First(), record as TableAeroscope);
                    //UpRecord(idClient, record, NameChangeOperation.Add);
                    UpDate(idClient);
                }
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }


        public void AddRange(ClassDataCommon data, int idClient)
        {
            try
            {
                foreach (var rec in data.ListRecords)
                {
                    string key = (string)rec.GetKey().FirstOrDefault();
                    if (TempTable.ContainsKey(key))
                        TempTable[key].Update(rec as TableAeroscope);
                    else
                        TempTable.Add(key, rec as TableAeroscope);
                }
                //UpAddRange(idClient, data);
                UpDate(idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }


        public void RemoveRange(ClassDataCommon data, int idClient)
        {
            try
            {
                foreach (var rec in data.ListRecords)
                {
                    string key = (string)rec.GetKey().FirstOrDefault();
                    if (TempTable.ContainsKey(key))
                    {
                        TempTable.Remove(key);
                        ClearTrajectory(key);
                    }
                }
                UpDate(idClient);

            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }


        public  void Change(AbstractCommonTable record, int idClient)
        {
            try
            {
                string key = (string)record.GetKey().FirstOrDefault();
                if (!TempTable.ContainsKey(key))
                {
                    throw new InheritorsException.ExceptionLocalDB(idClient, Errors.EnumDBError.RecordNotFound);
                }
                TempTable[key].Update(record as TableAeroscope);
                //UpRecord(idClient, record, NameChangeOperation.Change);
                UpDate(idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }


        public void Clear(int idClient)
        {
            try
            {
                foreach(var record in TempTable)
                {
                    ClearTrajectory(record.Key);
                }

                TempTable.Clear();
                UpDate(idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }


        public void Delete(AbstractCommonTable record, int idClient)
        {
            try
            {
                string key = (string)record.GetKey().FirstOrDefault();
                if (!TempTable.ContainsKey(key))
                {
                    throw new InheritorsException.ExceptionLocalDB(idClient, Errors.EnumDBError.RecordNotFound);
                }
                TempTable.Remove(key);
                UpRecord(idClient, record, NameChangeOperation.Delete);
                //UpDate(idClient);

                ClearTrajectory(key);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public void UpDate(int idClient)
        {
            try
            {
                base.SendUpData(this, new DataEventArgs(Name, ClassDataCommon.ConvertToListAbstractCommonTable(TempTable.Values.ToList())));//, idClient));
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }


        public void UpRecord(int idClient, AbstractCommonTable record, NameChangeOperation action)
        {
            try
            {
                base.SendUpRecord(this, new RecordEventArgs(Name, record, action));
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }


        public ClassDataCommon Load(int idClient)
        {
            ClassDataCommon data = new ClassDataCommon();
            try
            {
                //return ClassDataCommon.ConvertToListAbstractCommonTable(TempTable.Values.ToList());
                List<TableAeroscope> FullTable = new List<TableAeroscope>();
                foreach (var temps in TempTable.Values)
                {
                    FullTable.Add(temps);

                }
                return ClassDataCommon.ConvertToListAbstractCommonTable(FullTable);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }


        private void ClearTrajectory(string serialNum)
        {
            var listRemove = OperationTempAeroTraj.TempTable.Values.Where(t => t.SerialNumber == serialNum).ToList();
            foreach (var rec in listRemove)
            {
                OperationTempAeroTraj.TempTable.Remove(rec.Id);
            }
        }


        public void UpAddRange(int idClient, ClassDataCommon records)
        {
            try
            {
                base.SendRange(this, new DataEventArgs(Name, records));
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }
    }
}
