﻿using System;
using System.Linq;
using KirasaModelsDBLib;

namespace OperationsTablesLib
{
    public class OperationTempAeroTraj : OperationTempTable<TableAeroscopeTrajectory>
    {
        private static int lastIndex;
        public override void Add(AbstractCommonTable record, int idClient)
        {
            try
            {
                var listRecords = TempTable.Values.ToList();

                var findSerialNum = OperationTempAeroscope.TempTable.Values.Where(t => t.SerialNumber == (record as TableAeroscopeTrajectory).SerialNumber).ToList().Count;
                if (findSerialNum == 0)
                {
                    throw new InheritorsException.ExceptionLocalDB(idClient, Errors.EnumDBError.SuchAeroscopeAbsent);
                }

                if (listRecords.Count == 0)
                {
                    record.Id = 1;
                    lastIndex = 1;
                }
                else
                {
                    record.Id = ++lastIndex;
                }
                TempTable.Add((int)record.GetKey().FirstOrDefault(), record as TableAeroscopeTrajectory);
                //UpRecord(idClient, record, NameChangeOperation.Add);
                UpDate(idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }


        public override void AddRange(ClassDataCommon data, int idClient)
        {
            try
            {
                var listRecords = TempTable.Values.ToList();
                if (listRecords.Count == 0)
                {
                    lastIndex = 0;
                }

                foreach (var rec in data.ListRecords)
                {
                    rec.Id = ++lastIndex;
                    int key = (int)rec.GetKey().FirstOrDefault();

                    TempTable.Add(key, rec as TableAeroscopeTrajectory);
                }
                
                UpDate(idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }
    }
}
