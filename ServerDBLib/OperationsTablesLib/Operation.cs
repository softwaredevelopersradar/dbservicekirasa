﻿using System;
using KirasaModelsDBLib;
using InheritorsEventArgs;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Reflection;
//using System.Linq;

namespace OperationsTablesLib
{
    public class Operation
    {
        protected static TablesContext DataBase;

        protected NameTable Name;

        public bool IsTemp { get; protected set; }

        public static int TableLimit { get; protected set; }
        public static bool IsLimited { get; protected set; }
        public Operation()
        {
            IsTemp = false;
            if (DataBase != null) return;
            DataBase = new TablesContext();

            PropertyHost propertyHost = ReadProperty();
            TableLimit = propertyHost.TrajRecordsLimit;
            IsLimited = propertyHost.IsTrajTableLimited;
        }

        //~Operation()
        //{
        //    if (DataBase == null) return;
        //    DataBase.Dispose();
        //    DataBase = null;
        //}



        public static event EventHandler<DataEventArgs> OnReceiveData = (obj, eventArgs) => { };

        public static event EventHandler<DataEventArgs> OnAddRange = (obj, eventArgs) => { };

        public static event EventHandler<RecordEventArgs> OnReceiveRecord = (obj, eventArgs) => { };

        public static event EventHandler<string> OnSendMessToHost = (obj, mes) => { };


        internal void SendUpData(object obj, DataEventArgs data)
        {
            Task.Run(() => OnReceiveData(obj, data));
        }

        internal void SendRange(object obj, DataEventArgs data)
        {
            Task.Run(() => OnAddRange(obj, data));
        }

        internal void SendUpRecord(object obj, RecordEventArgs eventArgs)
        {
            Task.Run(() => OnReceiveRecord(obj, eventArgs));
        }

        internal void SendMessToHost(object obj, string mess)
        {
            Task.Run(() => OnSendMessToHost(obj, mess));
        }


        public static PropertyHost ReadProperty()
        {
            DataContractJsonSerializer jsonFormat = new DataContractJsonSerializer(typeof(PropertyHost));
            try
            {
                using (FileStream stream = new FileStream(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\propertyHost.json", FileMode.OpenOrCreate))
                {
                    PropertyHost propertyHost = (PropertyHost)jsonFormat.ReadObject(stream);
                    return propertyHost;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
