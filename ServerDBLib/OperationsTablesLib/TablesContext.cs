﻿using System;
using KirasaModelsDBLib;
using Microsoft.EntityFrameworkCore;

namespace OperationsTablesLib
{
    public class TablesContext : DbContext
    {
        #region DbSet
        public DbSet<TableLocalPoints> TLocalPoints { get; set; }
        public DbSet<TableRemotePoints> TRemotePoints { get; set; }
        public DbSet<TableFreqKnown> TFreqKnown { get; set; }
        public DbSet<TableFreqRangesRecon> TFreqRangesRecon { get; set; }
        public DbSet<GlobalProperties> TGlobalProperties { get; set; }
        public DbSet<TableUAVResArchive> TUAVResArchive { get; set; }
        public DbSet<TableUAVTrajectoryArchive> TUAVTrajectoryArchive { get; set; }
        public DbSet<TableSignalsUAV> TSignalsUAV { get; set; }
        public DbSet<TableOtherPoints> TOtherPoint { get; set; }
        public DbSet<TableSecurityGuard> TSecurityGuard { get; set; }
        #endregion

        public DbSet<T> GetTable<T>(NameTable name) where T : class
        {
            switch (name)
            {
                case NameTable.TableLocalPoints:
                    return TLocalPoints as DbSet<T>;
                case NameTable.TableRemotePoints:
                    return TRemotePoints as DbSet<T>;
                case NameTable.TableFreqRangesRecon:
                    return TFreqRangesRecon as DbSet<T>;
                case NameTable.TableFreqKnown:
                    return TFreqKnown as DbSet<T>;
                case NameTable.GlobalProperties:
                    return TGlobalProperties as DbSet<T>;
                case NameTable.TableUAVResArchive:
                    return TUAVResArchive as DbSet<T>;
                case NameTable.TableUAVTrajectoryArchive:
                    return TUAVTrajectoryArchive as DbSet<T>;
                case NameTable.TableSignalsUAV:
                    return TSignalsUAV as DbSet<T>;
                case NameTable.TableOtherPoints:
                    return TOtherPoint as DbSet<T>;
                case NameTable.TableSecurityGuard:
                    return TSecurityGuard as DbSet<T>;
                default:
                    return null;
            }
        }

        public TablesContext()
        {
            try
            {
               SQLitePCL.Batteries.Init();
            }
            catch (Exception ex)
            {
                var a = ex;
                Console.Write($"Error: {ex.Message}");
            }

            // SQLitePCL.raw.SetProvider(new SQLitePCL.SQLite3Provider_e_sqlite3());

            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //optionsBuilder.UseLazyLoadingProxies();
            optionsBuilder.EnableSensitiveDataLogging(true);
            var temp = System.IO.Directory.GetCurrentDirectory();
            var location = System.Reflection.Assembly.GetExecutingAssembly();
            string path = "Filename=" + System.IO.Directory.GetCurrentDirectory() + "\\KirasaDB.db";
            //string path = "Filename=" + System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\UniversalDB.db";
            optionsBuilder.UseSqlite(path);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Coord>().Property(rec => rec.Altitude).HasDefaultValue(-1);
            modelBuilder.Entity<Coord>().Property(rec => rec.Latitude).HasDefaultValue(-1);
            modelBuilder.Entity<Coord>().Property(rec => rec.Longitude).HasDefaultValue(-1);


            modelBuilder.Entity<TableLocalPoints>().Property(rec => rec.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<TableLocalPoints>().OwnsOne(t => t.Coordinates);


            modelBuilder.Entity<TableRemotePoints>().Property(rec => rec.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<TableRemotePoints>().OwnsOne(t => t.Coordinates);

            modelBuilder.Entity<TableOtherPoints>().Property(rec => rec.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<TableOtherPoints>().OwnsOne(t => t.Coordinates);


            modelBuilder.Entity<TableSecurityGuard>().Property(rec => rec.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<TableSecurityGuard>().OwnsOne(t => t.Coordinates);

            modelBuilder.Entity<Common>().Property(rec => rec.ZoneAttention).HasDefaultValue(5000);
            modelBuilder.Entity<Common>().Property(rec => rec.ZoneAlarm).HasDefaultValue(3000);

            modelBuilder.Entity<RadioIntelegence>().Property(rec => rec.FrequencyAccuracy).HasDefaultValue(0.1f);
            modelBuilder.Entity<RadioIntelegence>().Property(rec => rec.BandAccuracy).HasDefaultValue(0.1f);
            modelBuilder.Entity<RadioIntelegence>().Property(rec => rec.SamplingFrequency).HasDefaultValue(SamplingFrequency.f125e6);

            modelBuilder.Entity<KalmanFilterDelays>().Property(rec => rec.StaticErrorMatrix).HasDefaultValue(true);
            modelBuilder.Entity<KalmanFilterDelays>().Property(rec => rec.DelayMeasurementError).HasDefaultValue(8);
            modelBuilder.Entity<KalmanFilterDelays>().Property(rec => rec.AccelerationNoise).HasDefaultValue(4);
            modelBuilder.Entity<KalmanFilterDelays>().Property(rec => rec.TimeHoldToDetect).HasDefaultValue(3);
            modelBuilder.Entity<KalmanFilterDelays>().Property(rec => rec.MaxResetTime).HasDefaultValue(3);
            modelBuilder.Entity<KalmanFilterDelays>().Property(rec => rec.MinAirObjPoints).HasDefaultValue(5);

            modelBuilder.Entity<KalmanFilterCoordinates>().Property(rec => rec.SpeedUavMax).HasDefaultValue(20);
            modelBuilder.Entity<KalmanFilterCoordinates>().Property(rec => rec.TimeHoldToDetect).HasDefaultValue(10);
            modelBuilder.Entity<KalmanFilterCoordinates>().Property(rec => rec.MaxResetTime).HasDefaultValue(20);
            modelBuilder.Entity<KalmanFilterCoordinates>().Property(rec => rec.MinAirObjPoints).HasDefaultValue(3);
            modelBuilder.Entity<KalmanFilterCoordinates>().Property(rec => rec.StaticErrorMatrix).HasDefaultValue(true);
            modelBuilder.Entity<KalmanFilterCoordinates>().Property(rec => rec.RmsX).HasDefaultValue(0.1f);
            modelBuilder.Entity<KalmanFilterCoordinates>().Property(rec => rec.RmsY).HasDefaultValue(1);
            modelBuilder.Entity<KalmanFilterCoordinates>().Property(rec => rec.RmsZ).HasDefaultValue(0.1f);
            modelBuilder.Entity<KalmanFilterCoordinates>().Property(rec => rec.CoordinateDefinitionError).HasDefaultValue(80);
            modelBuilder.Entity<KalmanFilterCoordinates>().Property(rec => rec.StrobeDistance).HasDefaultValue(80);
            modelBuilder.Entity<KalmanFilterCoordinates>().Property(rec => rec.TauError).HasDefaultValue(3);
            modelBuilder.Entity<KalmanFilterCoordinates>().Property(rec => rec.XyzError).HasDefaultValue(0);

            modelBuilder.Entity<Correlation>().Property(rec => rec.AmountCorrelationQuery).HasDefaultValue(0);
            modelBuilder.Entity<Correlation>().Property(rec => rec.PathThroughCorrelationThreshold).HasDefaultValue(0.15f);
            modelBuilder.Entity<Correlation>().Property(rec => rec.CoefficientBorderDеlayEstimation).HasDefaultValue(0.8f);

            modelBuilder.Entity<DRMFilters>().Property(rec => rec.Alpha).HasDefaultValue(0.95f);
            modelBuilder.Entity<DRMFilters>().Property(rec => rec.IsImaginarySolution).HasDefaultValue(true);
            modelBuilder.Entity<DRMFilters>().Property(rec => rec.StartElevation).HasDefaultValue(50);

            modelBuilder.Entity<GlobalProperties>().OwnsOne(t => t.Common);
            modelBuilder.Entity<GlobalProperties>().OwnsOne(t => t.RadioIntelegence);
            modelBuilder.Entity<GlobalProperties>().OwnsOne(t => t.KalmanFilterForDelay);
            modelBuilder.Entity<GlobalProperties>().OwnsOne(t => t.KalmanFilterForCoordinate);
            modelBuilder.Entity<GlobalProperties>().OwnsOne(t => t.Correlation);
            modelBuilder.Entity<GlobalProperties>().OwnsOne(t => t.DRMFilters);

            modelBuilder.Entity<GlobalProperties>(b =>
            {
                b.HasData(new
                {
                    Id = 1
                });

                b.OwnsOne(e => e.Common).HasData(new
                {
                    GlobalPropertiesId = 1,
                    ZoneAttention = (short)5000,
                    ZoneAlarm = (short)3000,
                    CenterLatitude = (double)0,
                    CenterLongitude = (double)0,
                    OperatorLatitude = (double)0,
                    OperatorLongitude = (double)0
                });

                b.OwnsOne(e => e.RadioIntelegence).HasData(new
                {
                    GlobalPropertiesId = 1,                    
                    FrequencyAccuracy = (float)0.1f,
                    BandAccuracy = (float)1,
                    SamplingFrequency = SamplingFrequency.f125e6
                });

                b.OwnsOne(e => e.KalmanFilterForDelay).HasData(new
                {
                    GlobalPropertiesId = 1,
                    StaticErrorMatrix = true,
                    DelayMeasurementError = (float)8,
                    AccelerationNoise = (float)4,
                    TimeHoldToDetect = (float)3,
                    MaxResetTime = (float)3,
                    MinAirObjPoints = (short)5
                });

                b.OwnsOne(e => e.KalmanFilterForCoordinate).HasData(new
                {
                    GlobalPropertiesId = 1,
                    SpeedUavMax = (float)20,
                    TimeHoldToDetect = (float)10,
                    MaxResetTime = (float)20,
                    MinAirObjPoints = (short)3,
                    StaticErrorMatrix = true,
                    RmsX = (float)0.1,
                    RmsY = (float)1,
                    RmsZ = (float)0.1,
                    CoordinateDefinitionError = (float)80,
                    StrobeDistance = (float)80,
                    TauError = (short)3,
                    XyzError = (short)0
                });

                b.OwnsOne(e => e.Correlation).HasData(new
                {
                    GlobalPropertiesId = 1,
                    AmountCorrelationQuery = (byte)0,
                    PathThroughCorrelationThreshold = (float)0.15f,
                    CoefficientBorderDеlayEstimation = (float)0.8f
                });

                b.OwnsOne(e => e.DRMFilters).HasData(new
                {
                    GlobalPropertiesId = 1,
                    IsImaginarySolution = true,
                    Alpha = (float)0.95,
                    StartElevation = (short)50
                });
            });

           

            modelBuilder.Entity<TableUAVResArchive>().Property(rec => rec.Id).ValueGeneratedOnAdd();

            modelBuilder.Entity<TableUAVTrajectoryArchive>().Property(rec => rec.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<TableUAVTrajectoryArchive>().OwnsOne(t => t.Coordinates);
            modelBuilder.Entity<TableUAVTrajectoryArchive>().HasOne<TableUAVResArchive>().WithMany().HasForeignKey(rec => rec.TableUAVResId).OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<TableSignalsUAV>().Property(rec => rec.Id).ValueGeneratedOnAdd();
            
        }    

    }
}
