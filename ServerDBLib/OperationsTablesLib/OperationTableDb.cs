﻿using System;
using System.Linq;
using KirasaModelsDBLib;
using InheritorsEventArgs;
using Microsoft.EntityFrameworkCore;
using Errors;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OperationsTablesLib
{
    public class OperationTableDb<T> : Operation, ITableAction where T : AbstractCommonTable
    {
        // internal DbSet<T> Table;

        public OperationTableDb()
        {
            object[] attrs = typeof(T).GetCustomAttributes(typeof(InfoTableAttribute), false);
            foreach (InfoTableAttribute info in attrs)
            {
                Name = info.Name;
            }
            //Table = DataBase.GetTable<T>(Name);
        }

        public virtual void Add(AbstractCommonTable record, int idClient)
        {
            try
            {
                lock (DataBase)
                {
                    DbSet<T> Table = DataBase.GetTable<T>(Name);
                    if (Table.Find(record.GetKey()) != null)
                    {
                        throw new InheritorsException.ExceptionLocalDB(idClient, EnumDBError.RecordExist);
                    }
                    Table.Add(record as T);
                    DataBase.SaveChanges();
                }
                UpDate(idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                if (exception.InnerException != null)
                    throw exception.InnerException;
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public virtual void AddRange(ClassDataCommon data, int idClient)
        {
            try
            {
                lock (DataBase)
                {
                    DbSet<T> Table = DataBase.GetTable<T>(Name);

                    Table.AddRange(data.ToList<T>());
                    DataBase.SaveChanges();
                }
                UpDate(idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public virtual void Change(AbstractCommonTable record, int idClient)
        {
            try
            {
                lock (DataBase)
                {
                    DbSet<T> Table = DataBase.GetTable<T>(Name);

                    T rec = Table.Find(record.GetKey());
                    if (rec == null)
                        throw new InheritorsException.ExceptionLocalDB(idClient, EnumDBError.RecordNotFound);

                    (rec as AbstractCommonTable).Update(record);
                    Table.Update(rec);
                    DataBase.SaveChanges();
                }
                UpDate(idClient);
                return;
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public virtual void Clear(int idClient)
        {
            try
            {
                lock (DataBase)
                {
                    DbSet<T> Table = DataBase.GetTable<T>(Name);

                    Table.RemoveRange(Table.ToList());
                    DataBase.SaveChanges();
                }
                UpDate(idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public virtual void Delete(AbstractCommonTable record, int idClient)
        {
            try
            {
                lock (DataBase)
                {
                    DbSet<T> Table = DataBase.GetTable<T>(Name);

                    T rec = Table.Find(record.GetKey());
                    if (rec != null)
                    {
                        Table.Remove(rec);
                        DataBase.SaveChanges();
                    }
                    UpDate(idClient);
                    return;
                }
                throw new InheritorsException.ExceptionLocalDB(idClient, EnumDBError.RecordNotFound);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public virtual void RemoveRange(ClassDataCommon data, int idClient)
        {
            try
            {
                lock (DataBase)
                {
                    DbSet<T> Table = DataBase.GetTable<T>(Name);

                    List<T> rangeRemove = new List<T>();
                    foreach (var record in data.ToList<T>())
                    {
                        T rec = Table.Find(record.GetKey());
                        if (rec != null)
                            rangeRemove.Add(rec);
                    }
                    if (rangeRemove.Count == 0)
                        throw new InheritorsException.ExceptionLocalDB(idClient, EnumDBError.RecordNotFound);
                    Table.RemoveRange(rangeRemove);
                    DataBase.SaveChanges();
                }
                UpDate(idClient);
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public virtual ClassDataCommon Load(int idClient)
        {
            ClassDataCommon data = new ClassDataCommon();
            try
            {
                lock (DataBase)
                {
                    DbSet<T> Table = DataBase.GetTable<T>(Name);

                    // проверка на наличие вложенной таблицы 
                    var properties = typeof(T).GetProperties();
                    foreach (var property in properties)
                    {
                        if (property.PropertyType.GetInterface(typeof(IList).Name) != null && !property.PropertyType.IsArray)
                        {
                            // если найдена, то испльзуем жадную загрузку связных таблиц
                            return ClassDataCommon.ConvertToListAbstractCommonTable(Table.Include(property.Name).ToList());
                        }
                    }
                    return ClassDataCommon.ConvertToListAbstractCommonTable(Table.ToList());
                }
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public virtual void UpDate(int idClient)
        {
            try
            {
                Task.Run(() => base.SendUpData(this, new DataEventArgs(Name, Load(idClient))));//, idClient));
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public virtual void UpRecord(int idClient, AbstractCommonTable record, NameChangeOperation action)
        {
            try
            {
                base.SendUpRecord(this, new RecordEventArgs(Name, record, action));
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

    }
}
