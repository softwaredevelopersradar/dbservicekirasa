﻿using System;
using KirasaModelsDBLib;
using Microsoft.EntityFrameworkCore;
using Errors;
using InheritorsEventArgs;
using System.Collections.Generic;

namespace OperationsTablesLib
{
    public class OperationTableAeroscope : OperationTableDb<TableAeroscope>
    {
        public override void AddRange(ClassDataCommon data, int idClient)
        {
            try
            {

                List<TableAeroscope> AddRange = new List<TableAeroscope>();
                lock (DataBase)
                {
                    DbSet<TableAeroscope> Table = DataBase.GetTable<TableAeroscope>(Name);
                    foreach (var record in data.ListRecords)
                    {
                        var rec = Table.Find(record.GetKey());
                        if (rec != null)
                        {
                            rec.Update(record);
                            Table.Update(rec);
                            //if ((record as TableAeroscope).Trajectory != null)
                            //{
                            //    DbSet<TableAeroscopeTrajectory> TableAeroscopeTrajectory = DataBase.TАeroscopeTrajectory; //что будет если 2 раза назначить DBset
                            //    TableAeroscopeTrajectory.UpdateRange(rec.Trajectory);
                            //}
                            AddRange.Add(rec);
                        }
                        else
                        {
                            Table.Add(record as TableAeroscope);
                            AddRange.Add(record as TableAeroscope);
                        }
                    }
                    DataBase.SaveChanges();
                }
                UpDate(idClient);
                SendRange(this, new DataEventArgs(Name, ClassDataCommon.ConvertToListAbstractCommonTable(AddRange)));
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public override void Change(AbstractCommonTable record, int idClient)
        {
            try
            {
                List<TableAeroscope> ChangeRange = new List<TableAeroscope>();
                lock (DataBase)
                {
                    DbSet<TableAeroscope> Table = DataBase.GetTable<TableAeroscope>(Name);

                    TableAeroscope rec = Table.Find(record.GetKey());
                    if (rec == null)
                        throw new InheritorsException.ExceptionLocalDB(idClient, EnumDBError.RecordNotFound);

                    (rec as AbstractCommonTable).Update(record);
                    ChangeRange.Add(rec);
                    Table.Update(rec);
                    //if (rec.Trajectory != null)
                    //{
                    //    DbSet<TableAeroscopeTrajectory> TableAeroscopeTrajectory = DataBase.TАeroscopeTrajectory;
                    //    TableAeroscopeTrajectory.UpdateRange(rec.Trajectory);
                    //}
                    DataBase.SaveChanges();
                }

                //SendRange(this, new DataEventArgs(Name, ClassDataCommon.ConvertToListAbstractCommonTable(ChangeRange)));
                UpDate(idClient);
                return;
            }
            catch (InheritorsException.ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, exDb.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new InheritorsException.ExceptionLocalDB(idClient, ex.Message);
            }
        }
    }
}
