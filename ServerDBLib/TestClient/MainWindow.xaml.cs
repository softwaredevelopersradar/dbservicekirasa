﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using ClientDataBase;
using KirasaModelsDBLib;
using InheritorsEventArgs;
using System.IO;
using System.Collections.ObjectModel;
using System.Threading;
using System.Threading.Tasks;
using System.Text;

namespace TestClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ClientDB clientDB;
        string endPoint = "127.0.0.1:8302";

        public MainWindow()
        {
            InitializeComponent();
            this.Name = "TestClient";

        }

        public void DispatchIfNecessary(Action action)
        {
            if (!Dispatcher.CheckAccess())
                Dispatcher.Invoke(action);
            else
                action.Invoke();
        }


        void InitClientDB()
        {
            clientDB.OnConnect += HandlerConnect;
            clientDB.OnDisconnect += HandlerDisconnect;
            clientDB.OnUpData += HandlerUpData;
            clientDB.OnErrorDataBase += HandlerErrorDataBase;
            (clientDB.Tables[NameTable.TableАeroscopeTrajectory] as ITableUpdate<TableAeroscopeTrajectory>).OnUpTable += MainWindow_OnUpTable1;
            (clientDB.Tables[NameTable.TableАeroscopeTrajectory] as ITableUpRecord<TableAeroscopeTrajectory>).OnDeleteRecord += MainWindow_OnDeleteRecord2; ;
            (clientDB.Tables[NameTable.TableUAVRes] as ITableUpdate<TableUAVRes>).OnUpTable += MainWindow_OnUpTable;
            (clientDB.Tables[NameTable.TableUAVTrajectory] as ITableUpdate<TableUAVTrajectory>).OnUpTable += MainWindow_OnUpTable2;
            (clientDB.Tables[NameTable.TableUAVTrajectory] as ITableUpRecord<TableUAVTrajectory>).OnDeleteRecord += MainWindow_OnDeleteRecord1; ;
            (clientDB.Tables[NameTable.TableUAVRes] as ITableUpRecord<TableUAVRes>).OnDeleteRecord += MainWindow_OnDeleteRecord; ;
            (clientDB.Tables[NameTable.TableUAVRes] as ITableAddRange<TableUAVRes>).OnAddRange += MainWindow_OnAddRange;
            (clientDB.Tables[NameTable.TableАeroscope] as ITableUpRecord<TableAeroscope>).OnAddRecord += MainWindow_OnAddRecord;
            (clientDB.Tables[NameTable.TableАeroscope] as ITableUpRecord<TableAeroscope>).OnDeleteRecord += MainWindow_OnAddRecord;
            (clientDB.Tables[NameTable.TableSignalsUAV] as ITableUpRecord<TableSignalsUAV>).OnDeleteRecord += MainWindow_OnDeleteRecord3;
            (clientDB.Tables[NameTable.TableSignalsUAV] as ITableUpdate<TableSignalsUAV>).OnUpTable += MainWindow_OnUpTable3;
            (clientDB.Tables[NameTable.TableOtherPoints] as ITableUpdate<TableOtherPoints>).OnUpTable += MainWindow_OnUpTable4;
            //MainWindow_OnUpTable;
        }

        private void MainWindow_OnUpTable4(object sender, TableEventArgs<TableOtherPoints> e)
        {
            int f = 8;
        }

        private void MainWindow_OnUpTable3(object sender, TableEventArgs<TableSignalsUAV> e)
        {
            int f = 7;
        }

        private void MainWindow_OnDeleteRecord3(object sender, TableSignalsUAV e)
        {
            int f = 7;
        }

        private void MainWindow_OnDeleteRecord2(object sender, TableAeroscopeTrajectory e)
        {
            int f = 7;
            //throw new NotImplementedException();
        }

        private void MainWindow_OnDeleteRecord1(object sender, TableUAVTrajectory e)
        {
            int f = 7;
        }

        private void MainWindow_OnUpTable2(object sender, TableEventArgs<TableUAVTrajectory> e)
        {
            int f = 7;
        }

        private void MainWindow_OnDeleteRecord(object sender, TableUAVRes e)
        {
            //throw new NotImplementedException();
        }

        private void MainWindow_OnAddRange(object sender, TableEventArgs<TableUAVRes> e)
        {
            //throw new NotImplementedException();
        }

        private async void MainWindow_OnAddRecord(object sender, TableAeroscope e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                    tbMessage.AppendText($"Added SerialNum: {e.SerialNumber}, Id: {e.Id}\n");
            });
            dynamic table = await clientDB.Tables[NameTable.TableАeroscope].LoadAsync<KirasaModelsDBLib.TableAeroscope>();
            DispatchIfNecessary(() =>
            {
                tbMessage.AppendText($"Load data from Db. {(NameTable.TableАeroscope).ToString()} count records - {table.Count} \n");
            });
        }

        private void MainWindow_OnUpTable1(object sender, TableEventArgs<TableAeroscopeTrajectory> e)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                foreach (var rec in e.Table)
                {
                    tbMessage.AppendText($" TempAeroTraj: Id: {rec.Id}, SerialNum: {rec.SerialNumber}\n");
                }
            });
        }

        private async void MainWindow_OnUpTable(object sender, TableEventArgs<TableUAVRes> e)
        {

            dynamic table = await clientDB.Tables[NameTable.TableUAVRes].LoadAsync<KirasaModelsDBLib.TableUAVRes>();
            //tbMessage.Foreground = Brushes.Brown;
            DispatchIfNecessary(() =>
            {
                tbMessage.AppendText($" Db. Tables {NameTable.TableUAVRes} count records - {table.Count} or {e.Table.Count}\n");
            });
        }

        

        async void HandlerConnect(object obj, EventArgs eventArgs)
        {
            btnCon.Content = "Disconnect";

            dynamic table = await clientDB.Tables[NameTable.TableLocalPoints].LoadAsync<KirasaModelsDBLib.TableLocalPoints>();
            DispatchIfNecessary(() =>
            {
                tbMessage.AppendText($"Load data from Db. {(NameTable.TableLocalPoints).ToString()} count records - {table.Count} \n");
            });
           

            table = await clientDB.Tables[NameTable.TableRemotePoints].LoadAsync<KirasaModelsDBLib.TableRemotePoints>();
            DispatchIfNecessary(() =>
            {
                tbMessage.AppendText($"Load data from Db. {(NameTable.TableRemotePoints).ToString()} count records - {table.Count} \n");
            });
            

            table = await clientDB.Tables[NameTable.TableFreqKnown].LoadAsync<KirasaModelsDBLib.TableFreqKnown>();
            DispatchIfNecessary(() =>
            {
                tbMessage.AppendText($"Load data from Db. {(NameTable.TableFreqKnown).ToString()} count records - {table.Count} \n");
            });
            

            table = await clientDB.Tables[NameTable.TableFreqRangesRecon].LoadAsync<KirasaModelsDBLib.TableFreqRangesRecon>();
            DispatchIfNecessary(() =>
            {
                tbMessage.AppendText($"Load data from Db. {(NameTable.TableFreqRangesRecon).ToString()} count records - {table.Count} \n");
            });
            

            table = await clientDB.Tables[NameTable.TableUAVRes].LoadAsync<KirasaModelsDBLib.TableUAVRes>();
            DispatchIfNecessary(() =>
            {
                tbMessage.AppendText($"Load data from Db. {(NameTable.TableUAVRes).ToString()} count records - {table.Count} \n");
            });
            

            table = await clientDB.Tables[NameTable.TableUAVTrajectory].LoadAsync<KirasaModelsDBLib.TableUAVTrajectory>();
            DispatchIfNecessary(() =>
            {
                tbMessage.AppendText($"Load data from Db. {(NameTable.TableUAVTrajectory).ToString()} count records - {table.Count} \n");
            });
            

            table = await clientDB.Tables[NameTable.TableАeroscope].LoadAsync<KirasaModelsDBLib.TableAeroscope>();
            DispatchIfNecessary(() =>
            {
                tbMessage.AppendText($"Load data from Db. {(NameTable.TableАeroscope).ToString()} count records - {table.Count} \n");
            });
            

            table = await clientDB.Tables[NameTable.TableАeroscopeTrajectory].LoadAsync<KirasaModelsDBLib.TableAeroscopeTrajectory>();
            DispatchIfNecessary(() =>
            {
                tbMessage.AppendText($"Load data from Db. {(NameTable.TableАeroscopeTrajectory).ToString()} count records - {table.Count} \n");
            });

            table = await clientDB.Tables[NameTable.GlobalProperties].LoadAsync<KirasaModelsDBLib.GlobalProperties>();
            DispatchIfNecessary(() =>
            {
                tbMessage.AppendText($"Load data from Db. {(NameTable.GlobalProperties).ToString()} count records - {table.Count} \n");
            });


            table = await clientDB.Tables[NameTable.TableUAVResArchive].LoadAsync<TableUAVResArchive>();
            DispatchIfNecessary(() =>
            {
                tbMessage.AppendText($"Load data from Db. {NameTable.TableUAVResArchive} count records - {table.Count} \n");
            });


            table = await clientDB.Tables[NameTable.TableUAVTrajectoryArchive].LoadAsync<TableUAVTrajectoryArchive>();
            DispatchIfNecessary(() =>
            {
                tbMessage.AppendText($"Load data from Db. {NameTable.TableUAVTrajectoryArchive} count records - {table.Count} \n");
            });

            table = await clientDB.Tables[NameTable.TableSignalsUAV].LoadAsync<TableSignalsUAV>();
            DispatchIfNecessary(() =>
            {
                tbMessage.AppendText($"Load data from Db. {NameTable.TableSignalsUAV} count records - {table.Count} \n");
            });

            table = await clientDB.Tables[NameTable.TableOtherPoints].LoadAsync<TableOtherPoints>();
            DispatchIfNecessary(() =>
            {
                tbMessage.AppendText($"Load data from Db. {NameTable.TableOtherPoints} count records - {table.Count} \n");
            });

            table = await clientDB.Tables[NameTable.TableSecurityGuard].LoadAsync<TableSecurityGuard>();
            DispatchIfNecessary(() =>
                {
                    tbMessage.AppendText($"Load data from Db. {NameTable.TableSecurityGuard} count records - {table.Count} \n");
                });
        }

        void HandlerDisconnect(object obj, ClientEventArgs eventArgs)
        {
            btnCon.Content = "Connect";
            if (eventArgs.GetMessage != "")
            {
                tbMessage.Foreground = Brushes.Red;
                tbMessage.AppendText(eventArgs.GetMessage);
            }
            clientDB = null;
        }

        async void HandlerUpData(object obj, DataEventArgs eventArgs)
        {
            await Task.Run(() =>
            {
                Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
                {   
                    tbMessage.Foreground = Brushes.Black;

                    tbMessage.AppendText($"Load data from Db. Tables {eventArgs.Name.ToString()} count records - {eventArgs.AbstractData.ListRecords.Count} \n");

                    foreach (var rec in eventArgs.AbstractData.ListRecords)
                    {
                        tbMessage.AppendText($"Id: {rec.Id}\n");
                    }
                });
            });

        }

        void HandlerErrorDataBase(object obj, OperationTableEventArgs eventArgs)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                tbMessage.Foreground = Brushes.Red;
                tbMessage.AppendText(eventArgs.GetMessage);
            });
        }

        int index = 1;
        private async void ButAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int nufOfRec = Convert.ToInt32(RecordID.Text);
                Random rand = new Random();
                object record = null;
                switch (Tables.SelectedItem)
                {

                    case NameTable.TableLocalPoints:
                        record = new TableLocalPoints
                        {
                            Antenna = 20.3f,
                            Coordinates = new Coord
                            {
                                Altitude =(float)rand.NextDouble() * rand.Next(0, 360),
                                Latitude = rand.NextDouble() * rand.Next(0, 360),
                                Longitude = rand.NextDouble() * rand.Next(0, 360)
                            },
                            IsCetral = true, 
                            Note = "hello local",
                            TimeError = rand.Next(0, 250)
                            //Id = rand.Next(1, 1000)

                        };
                        break;
                    case NameTable.TableRemotePoints:
                        record = new TableRemotePoints
                        {
                            Antenna = 10.5f,
                            Coordinates = new Coord
                            {
                                Altitude = (float)rand.NextDouble() * rand.Next(0, 360),
                                Latitude = rand.NextDouble() * rand.Next(0, 360),
                                Longitude = rand.NextDouble() * rand.Next(0, 360)
                            },
                            Note = "dosvidos remote",
                            //Id = rand.Next(1, 1000)
                        };
                        break;
                     case NameTable.TableFreqKnown:
                        record = new TableFreqKnown
                        {
                            //Id = rand.Next(1, 10000),
                            FreqMaxKHz = rand.Next(150000, 250000),
                            FreqMinKHz = rand.Next(30000, 50000),
                        };
                        break;
                    case NameTable.TableFreqRangesRecon:
                        record = new TableFreqRangesRecon
                        {
                            //Id = rand.Next(1, 1000),
                            FreqMaxKHz = rand.Next(150000, 250000),
                            FreqMinKHz = rand.Next(30000, 50000),
                        };
                        break;
                    case NameTable.TableUAVRes:
                        record = new TableUAVRes
                        {
                            Id = index,
                            //FrequencyKHz = rand.NextDouble(),
                            //Elevation = rand.Next(0, 100),
                            //Coordinates = new Coord()
                            //{
                            //    Altitude = rand.Next(0, 100),
                            //    Latitude = rand.Next(0, 100),
                            //    Longitude = rand.Next(0, 100),
                            //},
                            //BandKHz = rand.Next(0, 100),
                            //Points = 50,
                            Note = DateTime.Now.ToString(),
                            IsActive = false, Flight = 10, TypeL = TypeL.Enemy, TypeM = TypeM.Drone
                            //Trajectory = new ObservableCollection<TableUAVTrajectory>()
                            //{
                            //    { new TableUAVTrajectory() { Coordinates = new Coord() { Altitude = rand.Next(0,360), Latitude = rand.Next(0,360), Longitude = rand.Next(0,360) }, Elevation = rand.Next(0, 100), Num = (short)rand.Next(0,50) } },
                            //    { new TableUAVTrajectory() { Coordinates = new Coord() { Altitude = rand.Next(0,360), Latitude = rand.Next(0,360), Longitude = rand.Next(0,360) }, Elevation = rand.Next(0, 100), Num = (short)rand.Next(0,50) } },
                            //    { new TableUAVTrajectory() { Coordinates = new Coord() { Altitude = rand.Next(0,360), Latitude = rand.Next(0,360), Longitude = rand.Next(0,360) }, Elevation = rand.Next(0, 100), Num = (short)rand.Next(0,50) } },
                            //    { new TableUAVTrajectory() { Coordinates = new Coord() { Altitude = rand.Next(0,360), Latitude = rand.Next(0,360), Longitude = rand.Next(0,360) }, Elevation = rand.Next(0, 100), Num = (short)rand.Next(0,50) } },
                            //    { new TableUAVTrajectory() { Coordinates = new Coord() { Altitude = rand.Next(0,360), Latitude = rand.Next(0,360), Longitude = rand.Next(0,360) }, Elevation = rand.Next(0, 100), Num = (short)rand.Next(0,50) } },
                            //    { new TableUAVTrajectory() { Coordinates = new Coord() { Altitude = rand.Next(0,360), Latitude = rand.Next(0,360), Longitude = rand.Next(0,360) }, Elevation = rand.Next(0, 100), Num = (short)rand.Next(0,50) } }
                            //},

                        };
                        break;
                    case NameTable.TableАeroscope:
                        record = new TableAeroscope
                        {
                            //Id = nufOfRec,
                            //SerialNumber = rand.Next(1, 100000).ToString(),
                            SerialNumber = RecordID.Text,
                            UUID = rand.Next(1, 10000000).ToString(),
                            UUIDLength = 8,
                            HomeLatitude = rand.NextDouble(),
                            HomeLongitude = rand.NextDouble(),
                            Type = "red",
                            IsActive = true
                            //Points = 50,
                            //Trajectory = new ObservableCollection<TableAeroscopeTrajectory>()
                            //{
                            //    { new TableAeroscopeTrajectory() { Coordinates = new Coord() { Altitude = rand.Next(0,360), Latitude = rand.Next(0,360), Longitude = rand.Next(0,360) }, Elevation = rand.Next(0, 100), Num = (short)rand.Next(0,50) } },
                            //    { new TableAeroscopeTrajectory() { Coordinates = new Coord() { Altitude = rand.Next(0,360), Latitude = rand.Next(0,360), Longitude = rand.Next(0,360) }, Elevation = rand.Next(0, 100), Num = (short)rand.Next(0,50) } },
                            //    { new TableAeroscopeTrajectory() { Coordinates = new Coord() { Altitude = rand.Next(0,360), Latitude = rand.Next(0,360), Longitude = rand.Next(0,360) }, Elevation = rand.Next(0, 100), Num = (short)rand.Next(0,50) } },
                            //    { new TableAeroscopeTrajectory() { Coordinates = new Coord() { Altitude = rand.Next(0,360), Latitude = rand.Next(0,360), Longitude = rand.Next(0,360) }, Elevation = rand.Next(0, 100), Num = (short)rand.Next(0,50) } },
                            //    { new TableAeroscopeTrajectory() { Coordinates = new Coord() { Altitude = rand.Next(0,360), Latitude = rand.Next(0,360), Longitude = rand.Next(0,360) }, Elevation = rand.Next(0, 100), Num = (short)rand.Next(0,50) } },
                            //    { new TableAeroscopeTrajectory() { Coordinates = new Coord() { Altitude = rand.Next(0,360), Latitude = rand.Next(0,360), Longitude = rand.Next(0,360) }, Elevation = rand.Next(0, 100), Num = (short)rand.Next(0,50) } }
                            //},

                        };
                        break;
                    case NameTable.TableUAVTrajectory:
                        record = new TableUAVTrajectory
                        {
                            Id = rand.Next(1, 100000),
                            Coordinates = new Coord() { Altitude = rand.Next(0, 360), Latitude = rand.Next(0, 360), Longitude = rand.Next(0, 360) },
                            Elevation = rand.Next(0, 100),
                            Num = (short)rand.Next(0, 50),
                            TableUAVResId = nufOfRec
                            //TableUAVRes = new TableUAVRes() { Id = rand.Next(1, 100000) }
                        };
                        break;
                    case NameTable.TableАeroscopeTrajectory:
                        record = new TableAeroscopeTrajectory
                        {
                            //Id = rand.Next(1, 100000),
                            Coordinates = new Coord() { Altitude = rand.Next(0, 360), Latitude = rand.Next(0, 360), Longitude = rand.Next(0, 360) },
                            Elevation = rand.Next(0, 100),
                            Num = (short)rand.Next(0, 50),
                            Roll = rand.Next(0, 100),
                            Pitch = rand.Next(0, 100),
                            Yaw = rand.Next(0, 100),
                            V_up = rand.Next(0, 100),
                            V_north = rand.Next(0, 100),
                            V_east = rand.Next(0, 100),
                            SerialNumber = RecordID.Text
                            //TableAeroscope = new TableAeroscope() { Id = rand.Next(1, 100000) }
                        };
                        break;
                    case NameTable.GlobalProperties:
                        record = new GlobalProperties
                        {
                            Id = 1,
                            //ZoneAlarm = (short)rand.Next(0, 100),
                            //ZoneAttention = (short)rand.Next(100, 250),
                            //CenterLatitude = rand.Next(0, 360),
                            //CenterLongitude = rand.Next(0, 360)
                        };
                        break;
                    case NameTable.TableUAVResArchive:
                        record = new TableUAVResArchive
                        {
                            Id = index,
                            Note = DateTime.Now.ToString(),
                            IsActive = false,
                            TypeL = TypeL.Enemy,
                            TypeM = TypeM.Drone
                        };
                        break;
                    case NameTable.TableUAVTrajectoryArchive:
                        record = new TableUAVTrajectoryArchive
                        {
                            //Id = rand.Next(1, 100000),
                            Coordinates = new Coord() { Altitude = rand.Next(0, 360), Latitude = rand.Next(0, 360), Longitude = rand.Next(0, 360) },
                            Elevation = rand.Next(0, 100),
                            Num = (short)rand.Next(0, 50),
                            TableUAVResId = nufOfRec
                        };
                        break;
                    case NameTable.TableSignalsUAV:
                        record = new TableSignalsUAV
                        {
                            TypeL = TypeL.Enemy,
                            TypeM = TypeM.Drone,
                        };
                        break;

                    case NameTable.TableOtherPoints:
                        record = new TableOtherPoints
                        {
                            Type = TypeOtherPoints.GrozaS,
                            Coordinates = new Coord() { Altitude = rand.Next(0, 360), Latitude = rand.Next(0, 360), Longitude = rand.Next(0, 360) }
                        };
                        break;
                    case NameTable.TableSecurityGuard:
                        record = new TableSecurityGuard
                        {
                                         GuardStation = "fhf",
                                         Coordinates = new Coord() { Altitude = rand.Next(0, 360), Latitude = rand.Next(0, 360), Longitude = rand.Next(0, 360) }
                                     };
                        break;
                    default:
                        return;
                }
                index++;
                if (record != null)
                    await clientDB?.Tables[(NameTable)Tables.SelectedItem].AddAsync(record);
            }
            catch (ClientDataBase.ExceptionClient exceptClient)
            {
                tbMessage.Foreground = Brushes.Red;
                tbMessage.AppendText(exceptClient.Message);
            }
        }

        private async void ButDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int nufOfRec = Convert.ToInt32(RecordID.Text);
                Random rand = new Random();
                object record = null;
                switch (Tables.SelectedItem)
                {

                    case NameTable.TableLocalPoints:
                        record = new TableLocalPoints
                        {
                            Id = nufOfRec
                        };
                        break;
                    case NameTable.TableRemotePoints:
                        record = new TableRemotePoints
                        {
                            Id = nufOfRec
                        };
                        break;
                    case NameTable.TableFreqKnown:
                        record = new TableFreqKnown
                        {
                            Id = nufOfRec
                        };
                        break;
                    case NameTable.TableFreqRangesRecon:
                        record = new TableFreqRangesRecon
                        {
                            Id = nufOfRec
                        };
                        break;
                    case NameTable.TableUAVRes:
                        record = new TableUAVRes
                        {
                            Id = nufOfRec
                        };
                        break;
                    case NameTable.TableАeroscope:
                        record = new TableAeroscope
                        {
                            SerialNumber = RecordID.Text
                        };
                        break;
                    case NameTable.TableUAVTrajectory:
                        record = new TableUAVTrajectory
                        {
                            Id = nufOfRec
                        };
                        break;
                    case NameTable.TableАeroscopeTrajectory:
                        record = new TableAeroscopeTrajectory
                        {
                            Id = nufOfRec
                        };
                        break;
                    case NameTable.GlobalProperties:
                        record = new GlobalProperties
                        {
                            Id = 1
                        };
                        break;
                    case NameTable.TableUAVResArchive:
                        record = new TableUAVResArchive
                        {
                            Id = nufOfRec
                        };
                        break;
                    case NameTable.TableUAVTrajectoryArchive:
                        record = new TableUAVTrajectoryArchive
                        {
                            Id = nufOfRec
                        };
                        break;
                    case NameTable.TableSignalsUAV:
                        record = new TableSignalsUAV
                        {
                            Id = nufOfRec
                        };
                        break;
                    case NameTable.TableOtherPoints:
                        record = new TableOtherPoints
                        {
                            Id = nufOfRec
                        };
                        break;
                    case NameTable.TableSecurityGuard:
                        record = new TableSecurityGuard
                        {
                                         Id = nufOfRec
                                     };
                        break;
                    default:
                        break;
                }
                if (record != null)
                    await clientDB?.Tables[(NameTable)Tables.SelectedItem].DeleteAsync(record);
            }
            catch (ClientDataBase.ExceptionClient exceptClient)
            {
                tbMessage.Foreground = Brushes.Red;
                tbMessage.AppendText(exceptClient.Message);
            }
        }

        private async void ButRemove_Click(object sender, RoutedEventArgs e)
        {
            int nufOfRec = Convert.ToInt32(RecordID.Text);
            Random rand = new Random();
            //object record = null;
            dynamic list = new object();
            switch (Tables.SelectedItem)
            {
                case NameTable.TableLocalPoints:
                    list = new List<TableLocalPoints>();
                    for (int i = nufOfRec; i < nufOfRec + 3; i++)
                    {
                        var record = new TableLocalPoints
                        {
                            Id = i
                        };
                        list.Add(record);
                    }
                    
                    break;
                case NameTable.TableRemotePoints:
                    list = new List<TableRemotePoints>();
                    for (int i = nufOfRec; i < nufOfRec + 3; i++)
                    {
                        var record = new TableRemotePoints
                        {
                            Id = nufOfRec
                        };
                        list.Add(record);
                    }
                    break;
                case NameTable.TableFreqKnown:
                    list = new List<TableFreqKnown>();
                    for (int i = nufOfRec; i < nufOfRec + 3; i++)
                    {
                        var record = new TableFreqKnown
                        {
                            Id = nufOfRec
                        };
                        list.Add(record);
                    }
                    break;
                case NameTable.TableFreqRangesRecon:
                    list = new List<TableFreqRangesRecon>();
                    for (int i = nufOfRec; i < nufOfRec + 3; i++)
                    {
                        var record = new TableFreqRangesRecon
                        {
                            Id = nufOfRec
                        };
                        list.Add(record);
                    }
                    break;
                case NameTable.TableUAVRes:
                    list = new List<TableUAVRes>();
                    for (int i = nufOfRec; i < nufOfRec + 3; i++)
                    {
                        var record = new TableUAVRes
                        {
                            Id = nufOfRec
                        };
                        list.Add(record);
                    }
                    break;
                case NameTable.TableАeroscope:
                    list = new List<TableAeroscope>();
                    for (int i = nufOfRec; i < nufOfRec + 3; i++)
                    {
                        var record = new TableAeroscope
                        {
                            Id = nufOfRec
                        };
                        list.Add(record);
                    }
                    break;
                case NameTable.TableUAVTrajectory:
                    list = new List<TableUAVTrajectory>();
                    for (int i = nufOfRec; i < nufOfRec + 3; i++)
                    {
                        var record = new TableUAVTrajectory
                        {
                            Id = nufOfRec
                        };
                        list.Add(record);
                    }
                    break;
                case NameTable.TableАeroscopeTrajectory:
                    list = new List<TableAeroscopeTrajectory>();
                    for (int i = nufOfRec; i < nufOfRec + 3; i++)
                    {
                        var record = new TableAeroscopeTrajectory
                        {
                            Id = nufOfRec
                        };
                        list.Add(record);
                    }
                    break;
                default:
                    break;


            }
            if (list != null)
                await clientDB?.Tables[(NameTable)Tables.SelectedItem].RemoveRangeAsync(list);
        }

        private async void SendRange_Click(object sender, RoutedEventArgs e)
        {
            try
            { 
                Random rand = new Random();
                //object record = null;
                switch (Tables.SelectedItem)
                {
                    case NameTable.TableLocalPoints:
                       List<TableLocalPoints> listForSend = new List<TableLocalPoints>();
                            for (int i = 0; i < 8; i++)
                            {
                                TableLocalPoints record = new TableLocalPoints
                                {
                                    Antenna = (float)rand.NextDouble(),
                                    Coordinates = new Coord
                                    {
                                        Altitude = (float)rand.NextDouble() * rand.Next(0, 360),
                                        Latitude = rand.NextDouble() * rand.Next(0, 360),
                                        Longitude = rand.NextDouble() * rand.Next(0, 360)
                                    },
                                    Note = "hello local",
                                    //Id = rand.Next(1, 1000)
                                };
                                listForSend.Add(record);
                            }
                            await clientDB.Tables[NameTable.TableLocalPoints].AddRangeAsync(listForSend);
                            break;
                    case NameTable.TableRemotePoints:

                            List<TableRemotePoints> listForSend1 = new List<TableRemotePoints>();
                            for (int i = 0; i < 8; i++)
                            {
                                TableRemotePoints record = new TableRemotePoints
                                {
                                    Antenna = 10.5f,
                                    Coordinates = new Coord
                                    {
                                        Altitude = (float)rand.NextDouble() * rand.Next(0, 360),
                                        Latitude = rand.NextDouble() * rand.Next(0, 360),
                                        Longitude = rand.NextDouble() * rand.Next(0, 360)
                                    },
                                    Note = "dosvidos remote",
                                    //Id = rand.Next(1, 1000)
                                };
                                listForSend1.Add(record);
                            }
                            await clientDB.Tables[NameTable.TableRemotePoints].AddRangeAsync(listForSend1);
                            break;
                    case NameTable.TableFreqKnown:
                            List<TableFreqKnown> listForSend2 = new List<TableFreqKnown>();
                            for (int i = 0; i < 8; i++)
                            {
                                TableFreqKnown record = new TableFreqKnown
                                {
                                    //Id = rand.Next(1, 10000),
                                    FreqMaxKHz = rand.Next(150000, 250000),
                                    FreqMinKHz = rand.Next(30000, 50000),
                                };
                                listForSend2.Add(record);
                            }
                            await clientDB.Tables[NameTable.TableFreqKnown].AddRangeAsync(listForSend2);
                            break;
                    case NameTable.TableFreqRangesRecon:
                            List<TableFreqRangesRecon> listForSend3 = new List<TableFreqRangesRecon>();
                            for (int i = 0; i < 8; i++)
                            {
                                TableFreqRangesRecon record = new TableFreqRangesRecon
                                {
                                    //Id = rand.Next(1, 1000),
                                    FreqMaxKHz = rand.Next(150000, 250000),
                                    FreqMinKHz = rand.Next(30000, 50000),
                                };
                                listForSend3.Add(record);
                            }
                            await clientDB.Tables[NameTable.TableFreqRangesRecon].AddRangeAsync(listForSend3);
                            break;
                    case NameTable.TableUAVRes:
                            List<TableUAVRes> listForSend4 = new List<TableUAVRes>();
                            for (int i = 0; i < 8; i++)
                            {
                                TableUAVRes record = new TableUAVRes
                                {
                                    //Id = rand.Next(9, 11),
                                    //FrequencyKHz = rand.NextDouble(),
                                    //Elevation = rand.Next(0, 100),
                                    //Coordinates = new Coord()
                                    //{
                                    //    Altitude = rand.Next(0, 100),
                                    //    Latitude = rand.Next(0, 100),
                                    //    Longitude = rand.Next(0, 100),
                                    //},
                                    //BandKHz = rand.Next(0, 100),
                                    //Points = 50,
                                    Note = DateTime.Now.ToString(),
                                    //Trajectory = new ObservableCollection<TableUAVTrajectory>()
                                    //    {
                                    //        { new TableUAVTrajectory() { Coordinates = new Coord() { Altitude = rand.Next(0,360), Latitude = rand.Next(0,360), Longitude = rand.Next(0,360) }, Elevation = rand.Next(0, 100), Num = (short)rand.Next(0,50) } },
                                    //        { new TableUAVTrajectory() { Coordinates = new Coord() { Altitude = rand.Next(0,360), Latitude = rand.Next(0,360), Longitude = rand.Next(0,360) }, Elevation = rand.Next(0, 100), Num = (short)rand.Next(0,50) } },
                                    //        { new TableUAVTrajectory() { Coordinates = new Coord() { Altitude = rand.Next(0,360), Latitude = rand.Next(0,360), Longitude = rand.Next(0,360) }, Elevation = rand.Next(0, 100), Num = (short)rand.Next(0,50) } },
                                    //        { new TableUAVTrajectory() { Coordinates = new Coord() { Altitude = rand.Next(0,360), Latitude = rand.Next(0,360), Longitude = rand.Next(0,360) }, Elevation = rand.Next(0, 100), Num = (short)rand.Next(0,50) } },
                                    //        { new TableUAVTrajectory() { Coordinates = new Coord() { Altitude = rand.Next(0,360), Latitude = rand.Next(0,360), Longitude = rand.Next(0,360) }, Elevation = rand.Next(0, 100), Num = (short)rand.Next(0,50) } },
                                    //        { new TableUAVTrajectory() { Coordinates = new Coord() { Altitude = rand.Next(0,360), Latitude = rand.Next(0,360), Longitude = rand.Next(0,360) }, Elevation = rand.Next(0, 100), Num = (short)rand.Next(0,50) } }
                                    //    },

                                };
                                listForSend4.Add(record);
                            }
                            await clientDB.Tables[NameTable.TableUAVRes].AddRangeAsync(listForSend4);
                            break;
                    case NameTable.TableАeroscope:
                            List<TableAeroscope> listForSend5 = new List<TableAeroscope>();
                            for (int i = 0; i < 8; i++)
                            {
                                TableAeroscope record = new TableAeroscope
                                {
                                    //Id = rand.Next(1, 100000),
                                    SerialNumber = rand.Next(1, 100000).ToString(),
                                    UUID = rand.Next(1, 10000000).ToString(),
                                    UUIDLength = 8,
                                    HomeLatitude = rand.NextDouble(),
                                    HomeLongitude = rand.NextDouble(),
                                    
                                    //Points = 50,
                                //    Trajectory = new ObservableCollection<TableAeroscopeTrajectory>()
                                //{
                                //    { new TableAeroscopeTrajectory() { Coordinates = new Coord() { Altitude = rand.Next(0,360), Latitude = rand.Next(0,360), Longitude = rand.Next(0,360) }, Elevation = rand.Next(0, 100), Num = (short)rand.Next(0,50) } },
                                //    { new TableAeroscopeTrajectory() { Coordinates = new Coord() { Altitude = rand.Next(0,360), Latitude = rand.Next(0,360), Longitude = rand.Next(0,360) }, Elevation = rand.Next(0, 100), Num = (short)rand.Next(0,50) } },
                                //    { new TableAeroscopeTrajectory() { Coordinates = new Coord() { Altitude = rand.Next(0,360), Latitude = rand.Next(0,360), Longitude = rand.Next(0,360) }, Elevation = rand.Next(0, 100), Num = (short)rand.Next(0,50) } },
                                //    { new TableAeroscopeTrajectory() { Coordinates = new Coord() { Altitude = rand.Next(0,360), Latitude = rand.Next(0,360), Longitude = rand.Next(0,360) }, Elevation = rand.Next(0, 100), Num = (short)rand.Next(0,50) } },
                                //    { new TableAeroscopeTrajectory() { Coordinates = new Coord() { Altitude = rand.Next(0,360), Latitude = rand.Next(0,360), Longitude = rand.Next(0,360) }, Elevation = rand.Next(0, 100), Num = (short)rand.Next(0,50) } },
                                //    { new TableAeroscopeTrajectory() { Coordinates = new Coord() { Altitude = rand.Next(0,360), Latitude = rand.Next(0,360), Longitude = rand.Next(0,360) }, Elevation = rand.Next(0, 100), Num = (short)rand.Next(0,50) } }
                                //},

                                };
                                listForSend5.Add(record);
                            }
                            await clientDB.Tables[NameTable.TableАeroscope].AddRangeAsync(listForSend5);
                            break;
                    case NameTable.TableUAVTrajectory:
                            List<TableUAVTrajectory> listForSend6 = new List<TableUAVTrajectory>();
                            for (int i = 0; i < 8; i++)
                            {
                                TableUAVTrajectory record = new TableUAVTrajectory
                                {
                                    //Id = rand.Next(1, 100000),
                                    Coordinates = new Coord() { Altitude = rand.Next(0, 360), Latitude = rand.Next(0, 360), Longitude = rand.Next(0, 360) },
                                    Elevation = rand.Next(0, 100),
                                    Num = (short)rand.Next(0, 50),
                                    TableUAVResId = 11
                                    //TableUAVRes = new TableUAVRes() { Id = rand.Next(1, 100000) }
                                };
                                listForSend6.Add(record);
                            }
                            await clientDB.Tables[NameTable.TableUAVTrajectory].AddRangeAsync(listForSend6);
                            break;
                    case NameTable.TableАeroscopeTrajectory:
                            List<TableAeroscopeTrajectory> listForSend7 = new List<TableAeroscopeTrajectory>();
                            for (int i = 0; i < 8; i++)
                            {
                                TableAeroscopeTrajectory record = new TableAeroscopeTrajectory
                                {
                                    //Id = rand.Next(1, 100000),
                                    Coordinates = new Coord() { Altitude = rand.Next(0, 360), Latitude = rand.Next(0, 360), Longitude = rand.Next(0, 360) },
                                    Elevation = rand.Next(0, 100),
                                    Num = (short)rand.Next(0, 50),
                                    Roll = rand.Next(0, 100),
                                    Pitch = rand.Next(0, 100),
                                    Yaw = rand.Next(0, 100),
                                    V_up = rand.Next(0, 100),
                                    V_north = rand.Next(0, 100),
                                    V_east = rand.Next(0, 100),
                                    //TableAeroscope = new TableAeroscope() { Id = rand.Next(1, 100000) }
                                };
                                listForSend7.Add(record);
                            }
                            await clientDB.Tables[NameTable.TableАeroscopeTrajectory].AddRangeAsync(listForSend7);
                            break;
                }
            
            }
            catch (ClientDataBase.ExceptionClient exceptClient)
            {
                tbMessage.Foreground = Brushes.Red;
                tbMessage.AppendText(exceptClient.Message);
            }
        }

        private async void ButClear_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                await clientDB?.Tables[(NameTable)Tables.SelectedItem].ClearAsync();
            }
            catch (ClientDataBase.ExceptionClient exceptClient)
            {
                Foreground = Brushes.Red;
                tbMessage.AppendText(exceptClient.Message);
            }
        }

        private async void ButLoad_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                tbMessage.Foreground = Brushes.Black;


                List<AbstractCommonTable> table = new List<AbstractCommonTable>();
                NameTable nameTable = (NameTable)Tables.SelectedItem;

                table =  await clientDB.Tables[nameTable].LoadAsync<AbstractCommonTable>();
                tbMessage.AppendText($"Load data from Db. {((NameTable)Tables.SelectedItem).ToString()} count records - {table.Count} \n");

            }
            catch (ClientDataBase.ExceptionClient exeptClient)
            {
                tbMessage.Foreground = Brushes.Red;
                tbMessage.AppendText(exeptClient.Message);
            }
            catch (ClientDataBase.ExceptionDatabase excpetService)
            {
                tbMessage.Foreground = Brushes.Red;
                tbMessage.AppendText(excpetService.Message);
            }
        }

        private async void butChange_Click(object sender, RoutedEventArgs e)
        {
            int nufOfRec = Convert.ToInt32(RecordID.Text);
            Random rand = new Random();
            object record = null;
            switch (Tables.SelectedItem)
            {

                case NameTable.TableLocalPoints:
                    record = new TableLocalPoints
                    {
                        Antenna = 10.3f,
                        Coordinates = new Coord
                        {
                            Altitude = (float)rand.NextDouble() * rand.Next(0, 360),
                            Latitude = rand.NextDouble() * rand.Next(0, 360),
                            Longitude = rand.NextDouble() * rand.Next(0, 360)
                        },
                        Note = "good local",
                        Id = nufOfRec,
                        TimeError = rand.Next(0, 250)

                    };
                    break;
                case NameTable.TableRemotePoints:
                    record = new TableRemotePoints
                    {
                        Antenna = 10.5f,
                        Coordinates = new Coord
                        {
                            Altitude = (float)rand.NextDouble() * rand.Next(0, 360),
                            Latitude = rand.NextDouble() * rand.Next(0, 360),
                            Longitude = rand.NextDouble() * rand.Next(0, 360)
                        },
                        Note = "dosvidos remote",
                        Id = nufOfRec
                    };
                    break;
                case NameTable.TableFreqKnown:
                    record = new TableFreqKnown
                    {
                        Id = nufOfRec,
                        FreqMaxKHz = rand.Next(150000, 250000),
                        FreqMinKHz = rand.Next(30000, 50000),
                    };
                    break;
                case NameTable.TableFreqRangesRecon:
                    record = new TableFreqRangesRecon
                    {
                        Id = nufOfRec,
                        FreqMaxKHz = rand.Next(150000, 250000),
                        FreqMinKHz = rand.Next(30000, 50000),
                    };
                    break;
                case NameTable.TableUAVRes:
                    record = new TableUAVRes
                    {
                        Id = nufOfRec,
                        //FrequencyKHz = rand.NextDouble(),
                        //Elevation = rand.Next(0, 100),
                        //Coordinates = new Coord()
                        //{
                        //    Altitude = rand.Next(0, 100),
                        //    Latitude = rand.Next(0, 100),
                        //    Longitude = rand.Next(0, 100),
                        //},
                        //BandKHz = rand.Next(0, 100),
                        //Points = 50,
                        IsActive = true,
                        Note = DateTime.Now.ToString(),
                        //Trajectory = new ObservableCollection<TableUAVTrajectory>()
                        //    {
                        //        { new TableUAVTrajectory() { Coordinates = new Coord() { Altitude = rand.Next(0,360), Latitude = rand.Next(0,360), Longitude = rand.Next(0,360) }, Elevation = rand.Next(0, 100), Num = (short)rand.Next(0,50) } },
                        //        { new TableUAVTrajectory() { Coordinates = new Coord() { Altitude = rand.Next(0,360), Latitude = rand.Next(0,360), Longitude = rand.Next(0,360) }, Elevation = rand.Next(0, 100), Num = (short)rand.Next(0,50) } },
                        //        { new TableUAVTrajectory() { Coordinates = new Coord() { Altitude = rand.Next(0,360), Latitude = rand.Next(0,360), Longitude = rand.Next(0,360) }, Elevation = rand.Next(0, 100), Num = (short)rand.Next(0,50) } },
                        //        { new TableUAVTrajectory() { Coordinates = new Coord() { Altitude = rand.Next(0,360), Latitude = rand.Next(0,360), Longitude = rand.Next(0,360) }, Elevation = rand.Next(0, 100), Num = (short)rand.Next(0,50) } },
                        //        { new TableUAVTrajectory() { Coordinates = new Coord() { Altitude = rand.Next(0,360), Latitude = rand.Next(0,360), Longitude = rand.Next(0,360) }, Elevation = rand.Next(0, 100), Num = (short)rand.Next(0,50) } },
                        //        { new TableUAVTrajectory() { Coordinates = new Coord() { Altitude = rand.Next(0,360), Latitude = rand.Next(0,360), Longitude = rand.Next(0,360) }, Elevation = rand.Next(0, 100), Num = (short)rand.Next(0,50) } }
                        //    },

                    };
                    break;
                case NameTable.TableАeroscope:
                    record = new TableAeroscope
                    {
                        SerialNumber = nufOfRec.ToString(),
                        UUID = rand.Next(1, 10000000).ToString(),
                        UUIDLength = 8,
                        HomeLatitude = rand.NextDouble(),
                        HomeLongitude = rand.NextDouble(),
                        Type = "green",
                        IsActive = false
                        //Points = 50,
                        //Trajectory = new ObservableCollection<TableAeroscopeTrajectory>()
                        //    {
                        //        { new TableAeroscopeTrajectory() { Coordinates = new Coord() { Altitude = rand.Next(0,360), Latitude = rand.Next(0,360), Longitude = rand.Next(0,360) }, Elevation = rand.Next(0, 100), Num = (short)rand.Next(0,50) } },
                        //        { new TableAeroscopeTrajectory() { Coordinates = new Coord() { Altitude = rand.Next(0,360), Latitude = rand.Next(0,360), Longitude = rand.Next(0,360) }, Elevation = rand.Next(0, 100), Num = (short)rand.Next(0,50) } },
                        //        { new TableAeroscopeTrajectory() { Coordinates = new Coord() { Altitude = rand.Next(0,360), Latitude = rand.Next(0,360), Longitude = rand.Next(0,360) }, Elevation = rand.Next(0, 100), Num = (short)rand.Next(0,50) } },
                        //        { new TableAeroscopeTrajectory() { Coordinates = new Coord() { Altitude = rand.Next(0,360), Latitude = rand.Next(0,360), Longitude = rand.Next(0,360) }, Elevation = rand.Next(0, 100), Num = (short)rand.Next(0,50) } },
                        //        { new TableAeroscopeTrajectory() { Coordinates = new Coord() { Altitude = rand.Next(0,360), Latitude = rand.Next(0,360), Longitude = rand.Next(0,360) }, Elevation = rand.Next(0, 100), Num = (short)rand.Next(0,50) } },
                        //        { new TableAeroscopeTrajectory() { Coordinates = new Coord() { Altitude = rand.Next(0,360), Latitude = rand.Next(0,360), Longitude = rand.Next(0,360) }, Elevation = rand.Next(0, 100), Num = (short)rand.Next(0,50) } }
                        //    },

                    };
                    break;
                case NameTable.TableUAVTrajectory:
                    record = new TableUAVTrajectory
                    {
                        Id = nufOfRec,
                        Coordinates = new Coord() { Altitude = rand.Next(0, 360), Latitude = rand.Next(0, 360), Longitude = rand.Next(0, 360) },
                        Elevation = rand.Next(0, 100),
                        Num = (short)rand.Next(0, 50),
                        //TableUAVResId = 10
                        //TableUAVRes = new TableUAVRes() { Id = rand.Next(1, 100000) }
                    };
                    break;
                case NameTable.TableАeroscopeTrajectory:
                    record = new TableAeroscopeTrajectory
                    {
                        Id = nufOfRec,
                        Coordinates = new Coord() { Altitude = rand.Next(0, 360), Latitude = rand.Next(0, 360), Longitude = rand.Next(0, 360) },
                        Elevation = rand.Next(0, 100),
                        Num = (short)rand.Next(0, 50),
                        Roll = rand.Next(0, 100),
                        Pitch = rand.Next(0, 100),
                        Yaw = rand.Next(0, 100),
                        V_up = rand.Next(0, 100),
                        V_north = rand.Next(0, 100),
                        V_east = rand.Next(0, 100),
                        //TableAeroscope = new TableAeroscope() { Id = rand.Next(1, 100000) }
                    };
                    break;
                case NameTable.GlobalProperties:
                    record = new GlobalProperties
                    {
                        Id = 1,
                        Common = new Common()
                        {
                            ZoneAlarm = (short)rand.Next(0, 100),
                            ZoneAttention = (short)rand.Next(100, 250),
                            CenterLatitude = rand.Next(0, 360),
                            CenterLongitude = rand.Next(0, 360)
                        }
                    };
                    break;
                case NameTable.TableUAVResArchive:
                    record = new TableUAVResArchive
                    {
                        Id = nufOfRec,
                        IsActive = true,
                        Note = DateTime.Now.ToString()
                    };
                    break;
                case NameTable.TableUAVTrajectoryArchive:
                    record = new TableUAVTrajectoryArchive
                    {
                        Id = nufOfRec,
                        Coordinates = new Coord() { Altitude = rand.Next(0, 360), Latitude = rand.Next(0, 360), Longitude = rand.Next(0, 360) },
                        Elevation = rand.Next(0, 100),
                        Num = (short)rand.Next(0, 50)
                    };
                    break;
                case NameTable.TableSignalsUAV:
                    record = new TableSignalsUAV
                    {
                        Id = nufOfRec,
                        TypeL = TypeL.Friend,
                        TypeM = TypeM.Unknown
                    };
                    break;
                case NameTable.TableOtherPoints:
                    record = new TableOtherPoints
                    {
                        Id = nufOfRec,
                        Coordinates = new Coord() { Altitude = rand.Next(0, 360), Latitude = rand.Next(0, 360), Longitude = rand.Next(0, 360) },
                    };
                    break;

            }
            if (record != null)
                await clientDB?.Tables[(NameTable)Tables.SelectedItem].ChangeAsync(record);
        }

        private void btnCon_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (clientDB != null)
                    clientDB.DisconnectAsync();
                else
                {
                    clientDB = new ClientDB(this.Name, endPoint);
                    InitClientDB();
                    clientDB.ConnectAsync();
                }
            }
            catch (ClientDataBase.ExceptionClient exceptClient)
            {
                tbMessage.Foreground = Brushes.Red;
                tbMessage.AppendText(exceptClient.Message);
            }
        }

        private void ButSimul_Click(object sender, RoutedEventArgs e)
        {
            

            taskADSB = new Task(sendADSBCycle);
            taskADSB.Start();
            return;
        }

    Task taskADSB;

        async void sendADSBCycle()
        {
            int nufOfRec = 1;
            Random rand = new Random();

            for (int i = 0; i < 20; i++)
            {
                var record = new TableUAVTrajectory
                {
                    Id = i,
                    Coordinates = new Coord() { Altitude = rand.Next(0, 360), Latitude = rand.Next(0, 360), Longitude = rand.Next(0, 360) },
                    Elevation = rand.Next(0, 100),
                    Num = (short)rand.Next(0, 50),
                    TableUAVResId = nufOfRec
                    //TableUAVRes = new TableUAVRes() { Id = rand.Next(1, 100000) }
                };
                await clientDB?.Tables[NameTable.TableUAVTrajectory].AddAsync(record);
                Thread.Sleep(10);
            }

        }

        private bool isSignalCycle = false;
        //Task taskSignals;
        async void sendSignalsUAVCycle()
        {
            Random rand = new Random();

            while (this.isSignalCycle)
            {
                var record = new TableSignalsUAV
                {
                     FrequencyKHz = rand.Next(700000,1000000),
                     BandKHz = rand.Next(2000, 20000),
                     Type = (TypeUAVRes)rand.Next(0, 5),
                     TypeL = (TypeL)rand.Next(0, 2),
                     TypeM = (TypeM)rand.Next(0, 2),
                     TimeStart = DateTime.Now,
                     TimeStop = DateTime.Now + TimeSpan.FromSeconds(1), 
                     Correlation = false,
                     System = TypeSystem.CP_CuirasseM,
                };
                await clientDB?.Tables[NameTable.TableSignalsUAV].AddAsync(record);
                Thread.Sleep(500);
            }

        }

        private void butSignalsUAV_Click(object sender, RoutedEventArgs e)
        {
            if (!this.isSignalCycle)
            {
                Task.Run(() => this.sendSignalsUAVCycle());
            }
            this.isSignalCycle = !this.isSignalCycle;
        }
    }
}
