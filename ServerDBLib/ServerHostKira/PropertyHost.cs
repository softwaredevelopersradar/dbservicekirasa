﻿using System.Runtime.Serialization;

namespace ServerHostKira
{
    [DataContract]
    public class PropertyHost
    {
        [DataMember]
        public string IpAddress { get; set; }

        [DataMember]
        public int PortHttp { get; set; }

        [DataMember]
        public int PortTcp { get; set; }

        [DataMember]
        public int TrajRecordsLimit { get; set; }

        [DataMember]
        public bool IsTrajTableLimited { get; set; }

        public PropertyHost(string ipAddress, int portHttp, int portTcp, int trajRecordsLimit, bool isTrajTableLimited)
        {
            IpAddress = ipAddress;
            PortHttp = portHttp;
            PortTcp = portTcp;
            TrajRecordsLimit = trajRecordsLimit;
            IsTrajTableLimited = isTrajTableLimited;
        }
    }
}
