﻿using System;
using System.ServiceModel;
using System.Runtime.InteropServices;
using InheritorsEventArgs;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Windows.Forms;
using System.Threading;
using System.Reflection;

namespace ServerHostKira
{
    class ServerHost
    {
        private static SignalHandler signalHandler;

        [STAThread]
        static void Main(string[] args)
        {
            // получаем GIUD приложения
            string guid = Marshal.GetTypeLibGuidForAssembly(Assembly.GetExecutingAssembly()).ToString();
            Mutex mutexObj = new Mutex(true, guid, out bool existed);
            if (!existed)
            {
                Console.WriteLine("The application is already running!");
                Thread.Sleep(3000);
                return;
            }

            Console.Title = "Kirasa ServerHost";
            signalHandler += HandleConsoleSignal;
            ConsoleHelper.SetSignalHandler(signalHandler, true);
            Console.CursorVisible = false;
            using (var host = InitializeServerHost())
            {
                if (host == null)
                {
                    Console.ReadKey();
                    Environment.Exit(0);
                }
                ServerDBLib.ServiceDB.OnReceiveErrorDB += ServiceDB_OnReceiveErrorDB;
                ServerDBLib.ServiceDB.OnReceiveMsgDB += ServiceDB_OnReceiveMsgDB;
                ServerDBLib.ServiceDB.OnErrorClient += ServiceDB_OnErrorClient;
                ServerDBLib.ServiceDB.OnStateClient += ServiceDB_OnStateClient;

                // ServerDBLib.ServiceDB.OnMessToHost += ServiceDB_OnMessToHost;
                new Control();
                // create new sync context, and WCF will use it, not WinForms context
                SynchronizationContext.SetSynchronizationContext(new SynchronizationContext());
                host.Open();
                Console.WriteLine("Kirasa DB server version 1.0 " + "\n");
                Console.WriteLine(host.BaseAddresses[1].AbsoluteUri + "\n");
                while (true)
                {
                    Console.ReadKey();
                }
            }
        }

        public delegate void SignalHandler(ConsoleSignal consoleSignal);

        public enum ConsoleSignal
        {
            CtrlC = 0,
            CtrlBreak = 1,
            Close = 2,
            LogOff = 5,
            Shutdown = 6
        }

        public static class ConsoleHelper
        {
            [DllImport("Kernel32", EntryPoint = "SetConsoleCtrlHandler")]
            public static extern bool SetSignalHandler(SignalHandler handler, bool add);
        }


        private static void HandleConsoleSignal(ConsoleSignal consoleSignal)
        {
            ServerDBLib.ServiceDB.TryDisconnectClients();
            Environment.Exit(0);
        }

        public static PropertyHost ReadProperty()
        {
            DataContractJsonSerializer jsonFormat = new DataContractJsonSerializer(typeof(PropertyHost));
            try
            {
                using (FileStream stream = new FileStream(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\propertyHost.json", FileMode.OpenOrCreate))
                {
                    PropertyHost propertyHost = (PropertyHost)jsonFormat.ReadObject(stream);
                    return propertyHost;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static PropertyHost CreateProperty()
        {
            PropertyHost propertyHost;
            DataContractJsonSerializer jsonFormat = new DataContractJsonSerializer(typeof(PropertyHost));
            try
            {
                string addresIp = "127.0.0.1";
                int portTCP = 8302;
                int portHTTP = 8301;
                int tableLimit = 1000;
                bool isLimited = true;
                propertyHost = new PropertyHost(addresIp, portHTTP, portTCP, tableLimit, isLimited);
                using (FileStream stream = new FileStream(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\propertyHost.json", FileMode.OpenOrCreate))
                {
                    jsonFormat.WriteObject(stream, propertyHost);
                }
                return propertyHost;
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write(ex.Message);
                return null;
            }
        }

        private static ServiceHost InitializeServerHost()
        {
            string address_HTTP;
            string address_TCP;
            PropertyHost propertyHost = ReadProperty();
            if (propertyHost == null)
            {
                propertyHost = CreateProperty();
            }

            address_HTTP = $"Http://{propertyHost.IpAddress}:{propertyHost.PortHttp}";
            address_TCP = $"net.tcp://{propertyHost.IpAddress}:{propertyHost.PortTcp}";

            Uri[] address_base = { new Uri(address_HTTP), new Uri(address_TCP) };
            ServiceHost serviceHost = new ServiceHost(typeof(ServerDBLib.ServiceDB), address_base)
            {
                CloseTimeout = TimeSpan.MaxValue
            };
            NetTcpBinding netTcpBinding = new NetTcpBinding(); // ("netTcpBinding");
            netTcpBinding.ReliableSession.Enabled = true;
            netTcpBinding.ReliableSession.InactivityTimeout =
                new TimeSpan(TimeSpan.MaxValue.Hours,
                TimeSpan.MaxValue.Minutes, TimeSpan.MaxValue.Seconds);
            netTcpBinding.ReliableSession.Ordered = true;
            netTcpBinding.ReceiveTimeout = TimeSpan.MaxValue;
            netTcpBinding.CloseTimeout = TimeSpan.MaxValue;
            netTcpBinding.SendTimeout = TimeSpan.FromSeconds(20); //TimeSpan.MaxValue;
            netTcpBinding.Security.Mode = SecurityMode.None;
            netTcpBinding.MaxBufferPoolSize = Int64.MaxValue;
            netTcpBinding.MaxReceivedMessageSize = 100000000;//100MB
            netTcpBinding.MaxBufferSize = 100000000;//100MB
            netTcpBinding.MaxConnections = 10000;
            netTcpBinding.ListenBacklog = 10000;


            serviceHost.AddServiceEndpoint(typeof(ServerDBLib.IServiceDB), netTcpBinding, "");
            return serviceHost;
        }

        private static void ServiceDB_OnReceiveMsgDB(object sender, AStandardEventArgs e)
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.BackgroundColor = ConsoleColor.Black;
            Console.WriteLine(e.GetMessage);
        }

        private static void ServiceDB_OnReceiveErrorDB(object sender, AStandardEventArgs e)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.BackgroundColor = ConsoleColor.Black;
            Console.WriteLine(e.GetMessage);
        }

        private static void ServiceDB_OnStateClient(object sender, AStandardEventArgs e)
        {
            Console.BackgroundColor = ConsoleColor.Blue;
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(e.GetMessage);
        }

        private static void ServiceDB_OnErrorClient(object sender, AStandardEventArgs e)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.BackgroundColor = ConsoleColor.Black;
            Console.WriteLine(e.GetMessage);
        }

        private static void ServiceDB_OnMessToHost(object sender, string e)
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.BackgroundColor = ConsoleColor.Black;
            Console.WriteLine(e);
        }
    }
}
